-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 20, 2018 at 01:12 AM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 7.2.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `register`
--

-- --------------------------------------------------------

--
-- Table structure for table `biodata`
--

CREATE TABLE `biodata` (
  `nik` varchar(30) NOT NULL,
  `id_pekerjaan` int(11) NOT NULL,
  `id_pendidikan` int(11) NOT NULL,
  `nama_lengkap` varchar(50) NOT NULL,
  `tmpt_lahir` varchar(30) NOT NULL,
  `tgl_lahir` date NOT NULL,
  `jk` varchar(10) NOT NULL,
  `kewarganegaraan` varchar(15) NOT NULL,
  `tmpt_tinggal` varchar(50) NOT NULL,
  `agama` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `biodata`
--

INSERT INTO `biodata` (`nik`, `id_pekerjaan`, `id_pendidikan`, `nama_lengkap`, `tmpt_lahir`, `tgl_lahir`, `jk`, `kewarganegaraan`, `tmpt_tinggal`, `agama`, `created_at`) VALUES
('1111', 1, 6, '111', '111', '2018-01-17', 'Laki-Laki', '111', '111', 0, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `pangkat_gol`
--

CREATE TABLE `pangkat_gol` (
  `id_pangkat_gol` int(11) NOT NULL,
  `gol` varchar(20) NOT NULL,
  `pangkat` varchar(20) NOT NULL,
  `urutan` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pangkat_gol`
--

INSERT INTO `pangkat_gol` (`id_pangkat_gol`, `gol`, `pangkat`, `urutan`) VALUES
(1, 'II/A', 'Yuana Darma/Pengatur', 0),
(2, 'II/B', 'Muda Darma/Pengatur ', 2);

-- --------------------------------------------------------

--
-- Table structure for table `pegawai`
--

CREATE TABLE `pegawai` (
  `id_pegawai` int(11) NOT NULL,
  `nip` varchar(25) NOT NULL,
  `nama_pegawai` varchar(50) NOT NULL,
  `id_pangkat_gol` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pegawai`
--

INSERT INTO `pegawai` (`id_pegawai`, `nip`, `nama_pegawai`, `id_pangkat_gol`) VALUES
(1, '19109101910910', 'samsi', 1);

-- --------------------------------------------------------

--
-- Table structure for table `pekerjaan`
--

CREATE TABLE `pekerjaan` (
  `id_pekerjaan` int(11) NOT NULL,
  `nama_pekerjaan` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pekerjaan`
--

INSERT INTO `pekerjaan` (`id_pekerjaan`, `nama_pekerjaan`) VALUES
(1, 'Tidak Bekerja');

-- --------------------------------------------------------

--
-- Table structure for table `pendidikan`
--

CREATE TABLE `pendidikan` (
  `id_pendidikan` int(11) NOT NULL,
  `nama_pendidikan` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pendidikan`
--

INSERT INTO `pendidikan` (`id_pendidikan`, `nama_pendidikan`) VALUES
(1, 'Tidak Sekolah'),
(2, 'TK'),
(3, 'SD'),
(4, 'SMP'),
(5, 'SMA'),
(6, 'DI'),
(7, 'DII'),
(8, 'DIII'),
(9, 'DIV'),
(10, 'S1'),
(11, 'S2'),
(12, 'S3');

-- --------------------------------------------------------

--
-- Table structure for table `rb1`
--

CREATE TABLE `rb1` (
  `no_rb1` int(11) NOT NULL,
  `tgl` date NOT NULL,
  `no_rp3` smallint(11) NOT NULL,
  `jumlah_sitaan` varchar(10) NOT NULL,
  `jenis_sitaan` varchar(50) NOT NULL,
  `tmpt_penyimpanan` varchar(50) NOT NULL,
  `no_penyitaan` varchar(50) NOT NULL,
  `tgl_penyitaan` date NOT NULL,
  `petugas_penyitaan` varchar(50) NOT NULL,
  `disita_dari` varchar(50) NOT NULL,
  `no_persetujuan` varchar(50) NOT NULL,
  `tgl_persetujuan` date NOT NULL,
  `no_sp_lelang` varchar(50) NOT NULL,
  `tgl_sp_lelang` date NOT NULL,
  `no_penetapan` varchar(50) NOT NULL,
  `tgl_penetapan` date NOT NULL,
  `jmlh_hsl_lelang` varchar(50) NOT NULL,
  `sitaan_disisihkan` varchar(50) NOT NULL,
  `keterangan` varchar(100) NOT NULL,
  `tgl_simpan` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rb2`
--

CREATE TABLE `rb2` (
  `no_rb2` varchar(20) NOT NULL,
  `reg_perkara` date NOT NULL,
  `tgl_penerimaan_tgg_jwb` varchar(50) NOT NULL,
  `jpu` varchar(50) NOT NULL,
  `nik` varchar(30) NOT NULL,
  `psl_didakwakan` varchar(100) NOT NULL,
  `jml_ukrn_jns` varchar(50) NOT NULL,
  `penyimpanan` date NOT NULL,
  `tgl_penyerahan` varchar(50) NOT NULL,
  `no_putusan_pn` varchar(50) NOT NULL,
  `tgl_putusan_pn` varchar(50) NOT NULL,
  `no_putusan_pt` varchar(50) NOT NULL,
  `tgl_putusan_pt` date NOT NULL,
  `no_putusan_ma` varchar(50) NOT NULL,
  `tgl_putusan_ma` date NOT NULL,
  `tgl_ba_plksanaan_ptusan` varchar(50) NOT NULL,
  `no_penguguman` varchar(50) NOT NULL,
  `tgl_pengumuman` date NOT NULL,
  `no_ijin_lelang` varchar(50) NOT NULL,
  `tgl_ijin_lelang` date NOT NULL,
  `tgl_ba_penyerahan` date NOT NULL,
  `no_pemanfaatan` varchar(50) NOT NULL,
  `tgl_pemanfaatan` date NOT NULL,
  `tgl_ba_instansi` date NOT NULL,
  `no_pengumuman_bt` varchar(50) NOT NULL,
  `tgl_pengumuman_bt` date NOT NULL,
  `no_izin_lelang` varchar(50) NOT NULL,
  `tgl_izin_lelang` date NOT NULL,
  `tgl_ba_penyerahan_bt` date NOT NULL,
  `no_pemanfaatan_bt` varchar(50) NOT NULL,
  `tgl_pemanfaatan_bt` date NOT NULL,
  `keterangan` varchar(100) NOT NULL,
  `tgl_simpan` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rp1`
--

CREATE TABLE `rp1` (
  `no_rp1` varchar(20) NOT NULL,
  `nik` varchar(30) NOT NULL,
  `nama_penerima` varchar(50) NOT NULL,
  `tgl_penerimaan_laporan` date NOT NULL,
  `isi_ringkas` varchar(100) NOT NULL,
  `diteruskan` varchar(100) NOT NULL,
  `keterangan` varchar(100) NOT NULL,
  `tgl_simpan` date NOT NULL,
  `no_agenda` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rp2`
--

CREATE TABLE `rp2` (
  `no_rp2` varchar(20) NOT NULL,
  `tgl_penyelidikan` varchar(30) NOT NULL,
  `kasus` varchar(50) NOT NULL,
  `id_pegawai` date NOT NULL,
  `nama_pelapor` varchar(100) NOT NULL,
  `identitas` varchar(100) NOT NULL,
  `tgl_ditutup` varchar(100) NOT NULL,
  `tgl_ditingkatkan` date NOT NULL,
  `keterangan` varchar(100) NOT NULL,
  `tgl_simpan` date NOT NULL,
  `no_agenda` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rp3`
--

CREATE TABLE `rp3` (
  `no_rp3` varchar(20) NOT NULL,
  `tgl_reg_perkara` date NOT NULL,
  `asal_perkara` varchar(100) NOT NULL,
  `nik` int(11) NOT NULL,
  `kasus` varchar(100) NOT NULL,
  `status_tsk` varchar(100) NOT NULL,
  `no_sp_penahanan` varchar(100) NOT NULL,
  `tgl_sp_penahanan` date NOT NULL,
  `no_denda_sitaan` varchar(50) NOT NULL,
  `tgl_denda_sitaan` date NOT NULL,
  `id_pegawai` int(11) NOT NULL,
  `no_penghentian_dik` varchar(50) NOT NULL,
  `keterangan` varchar(100) NOT NULL,
  `tgl_simpan` date NOT NULL,
  `no_agenda` int(11) NOT NULL,
  `tgl_penghentian_dik` date NOT NULL,
  `pengalihan` varchar(50) NOT NULL,
  `no_pelimpahan` varchar(50) NOT NULL,
  `tgl_pelimpahan` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rp4`
--

CREATE TABLE `rp4` (
  `no_rp4` varchar(20) NOT NULL,
  `tgl` date NOT NULL,
  `alamat_dipanggil` varchar(100) NOT NULL,
  `alasan_pemanggilan` varchar(100) NOT NULL,
  `no_rp3` varchar(20) NOT NULL,
  `tmpt_menghadap` varchar(100) NOT NULL,
  `tgl_menghadap` date NOT NULL,
  `jam_menghadap` varchar(5) NOT NULL,
  `menghadap_kpd` varchar(50) NOT NULL,
  `keterangan` varchar(100) NOT NULL,
  `tgl_simpan` date NOT NULL,
  `no_agenda` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rt1`
--

CREATE TABLE `rt1` (
  `no_rt1` varchar(20) NOT NULL,
  `nik` varchar(30) NOT NULL,
  `no_rp3` varchar(20) NOT NULL,
  `pasal_disangkakan` varchar(50) NOT NULL,
  `jns_penahanan` varchar(50) NOT NULL,
  `tgl_ditahan` date NOT NULL,
  `tgl_24kuhap_sejak` date NOT NULL,
  `tgl_29kuhap_sejak` date NOT NULL,
  `tgl_pengalihan` date NOT NULL,
  `dikeluarkan_demi_hukum` varchar(50) NOT NULL,
  `no_pengguhan` varchar(50) NOT NULL,
  `tgl_penangguhan` date NOT NULL,
  `jaminan_penangguhan` varchar(50) NOT NULL,
  `tgl_pencabutan_penangguhan` date NOT NULL,
  `penyelesaian` varchar(50) NOT NULL,
  `keterangan` text NOT NULL,
  `tgl_simpan` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rt2`
--

CREATE TABLE `rt2` (
  `no_rt2` varchar(20) NOT NULL,
  `no_perpanjangan` varchar(30) NOT NULL,
  `tgl_ketetapan_perpanjangan` varchar(20) NOT NULL,
  `nik` varchar(50) NOT NULL,
  `pasal_yg_disangkakan` varchar(50) NOT NULL,
  `nomor` varchar(50) NOT NULL,
  `tgl` date NOT NULL,
  `tgl_ditahan` date NOT NULL,
  `tgl_perpanjangan` date NOT NULL,
  `tgl_perpanjangan2` date NOT NULL,
  `ditangguhkan` varchar(50) NOT NULL,
  `keterangan` text NOT NULL,
  `tgl_simpan` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id_user` int(11) NOT NULL,
  `username` varchar(30) NOT NULL,
  `password` varchar(30) NOT NULL,
  `nickname` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id_user`, `username`, `password`, `nickname`) VALUES
(1, 'admin', 'admin', 'admin');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `biodata`
--
ALTER TABLE `biodata`
  ADD PRIMARY KEY (`nik`);

--
-- Indexes for table `pangkat_gol`
--
ALTER TABLE `pangkat_gol`
  ADD PRIMARY KEY (`id_pangkat_gol`);

--
-- Indexes for table `pegawai`
--
ALTER TABLE `pegawai`
  ADD PRIMARY KEY (`id_pegawai`);

--
-- Indexes for table `pekerjaan`
--
ALTER TABLE `pekerjaan`
  ADD PRIMARY KEY (`id_pekerjaan`);

--
-- Indexes for table `pendidikan`
--
ALTER TABLE `pendidikan`
  ADD PRIMARY KEY (`id_pendidikan`);

--
-- Indexes for table `rb1`
--
ALTER TABLE `rb1`
  ADD PRIMARY KEY (`no_rb1`);

--
-- Indexes for table `rb2`
--
ALTER TABLE `rb2`
  ADD PRIMARY KEY (`no_rb2`);

--
-- Indexes for table `rp1`
--
ALTER TABLE `rp1`
  ADD PRIMARY KEY (`no_rp1`);

--
-- Indexes for table `rp2`
--
ALTER TABLE `rp2`
  ADD PRIMARY KEY (`no_rp2`);

--
-- Indexes for table `rp3`
--
ALTER TABLE `rp3`
  ADD PRIMARY KEY (`no_rp3`);

--
-- Indexes for table `rp4`
--
ALTER TABLE `rp4`
  ADD PRIMARY KEY (`no_rp4`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `pangkat_gol`
--
ALTER TABLE `pangkat_gol`
  MODIFY `id_pangkat_gol` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `pegawai`
--
ALTER TABLE `pegawai`
  MODIFY `id_pegawai` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `pekerjaan`
--
ALTER TABLE `pekerjaan`
  MODIFY `id_pekerjaan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `pendidikan`
--
ALTER TABLE `pendidikan`
  MODIFY `id_pendidikan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
