<?php
    function templates($a=''){
        return base_url('assets/AdminLTE-3.0.0-alpha/'.$a);
    }
    function namePage($a){
        $nfw=&get_instance();

        switch ($a) {
            case 'user':
                return 'Pengguna';
                break;
            
            case 'rb1':
                return rbConvert(1,1).' ['.rbConvert(1,0).']';
                break;

            case 'rb2':
                return rbConvert(2,1).' ['.rbConvert(2,0).']';
                break;
            case 'rp1':
                return rpConvert(1,1).' ['.rpConvert(1,0).']';
                break;
            
            case 'rp2':
                return rpConvert(2,1).' ['.rpConvert(2,0).']';
                break;
            
            case 'rp3':
                return rpConvert(3,1).' ['.rpConvert(3,0).']';
                break;
            
            case 'rp4':
                return rpConvert(4,1).' ['.rpConvert(4,0).']';
                break;
                        
            case 'kirimitem':
                  return 'Pengiriman Item (DOC, OVK & Pakan)';
                break;
            
            case 'home':
                return 'Beranda';
                break;
            
            case 'mati':
                return 'Ayam Mati';
                break;
            
            case 'blpakan':
                return 'Pembelian Pakan';
                break;
            
            case 'blovk':
                return 'Pembelian OVK';
                break;
            
            case 'jual':
                return 'Penjualan';
                break;
            
            case 'konbw':
                return 'Kontrak Body Weight';
                break;

            case '':
                return 'Beranda';
                break;
            
            default:
                return $a;
                break;
        }
    }
    function rtConvert($i,$j){  
        switch ($i) {
            case '1':
                $a=['RT1','Tahap Penyidik'];
                break;
            
            case '2':
                $a=['RT2','Perpanjangan Penahanan'];
                break;
            
            case '3':
                $a=['RT3','Tahanan Perkara Tahap Penyidikan'];
                break;
            
            default:
                $a=['RT1','Tahap Penyidik'];
                break;
        }
        return $a[$j];

    }
    function rbConvert($i,$j){  
        switch ($i) {
            case '1':
                $a=['RB1','Benda Sitaan'];
                break;
            
            case '2':
                $a=['RB2','Barang Bukti dan Barang Temuan'];
                break;
            
            default:
                $a=['RB1','Benda Sitaan'];
                break;
        }
        return $a[$j];
    }
    function rpConvert($i,$j){
        switch ($i) {
            case '1':
                $a=['RP1','Penerimaan Laporan'];
                break;
            
            case '2':
                $a=['RP2','Tahap Penyeledikan'];
                break;
            
            case '3':
                $a=['RP3','Tahap Penyidik'];
                break;
            
            case '4':
                $a=['RP4','Permintaan Keterangan'];
                break;
            
            case '5':
                $a=['RP5','Jaksa Kegiatan Penyidikan'];
                break;
            
            case '6':
                $a=['RP6','Pemberitahuan dimulainya penyidikan/dihentikannya penyidikan'];
                break;
            
            case '7':
                $a=['RP7','Penerimaan Berkas Perkara'];
                break;
            
            case '8':
                $a=['RP8','Pemeriksaan Tambahan'];
                break;
            
            case '9':
                $a=['RP9','Pemeriksaan Tambahan'];
                break;
            
            case '10':
                $a=['RP10','Penghentian Tuntutan dan Penyampingan Perkara demi Kepentingan Umum'];
                break;
            
            case '11':
                $a=['RP11','Upaya Hukum dan Grasi'];
                break;
            
            case '12':
                $a=['RP12','Pelaksanaan Putusan'];
                break;
            
            case '13':
                $a=['RP13','Permintaan Keterangan/Panggilan'];
                break;
            
            case '14':
                $a=['RP14','Buku Perkara Jaksa Penuntut Umum'];
                break;
            
            default:
                $a=['RP14','Penerimaan Laporan'];
                break;
                
            }
        return $a[$j];
    }
    function bulanList(){
        $op=array();
        $op['']='--Semua Bulan--';
        for ($i=1; $i <=12 ; $i++) {
            if(strlen($i)==1){
                $i='0'.$i;
            } 
            $op[$i]=bulan_huruf($i);
        }
        return $op;
    }
    function tahunList($a='',$b=''){
        if($a==''){
            $a=date('Y')-5;
        }
        if($b==''){
            $b=$a+10;
        }
        $op=array();
        $op['']='--Semua Tahun--';
        for ($i=$a; $i <=$b ; $i++) { 
            $op[$i]=$i;
        }
        return $op;
    }
    function setList($a,$b,$c){
        $op=array();
        $op['']='--Pilih Semua--';
        foreach ($a as $r) {
            $op[$r->$b]=$r->$c;
        }
        return $op;
    }
    function kontrak($a='id_kontrak'){
        $nfw=&get_instance();
        if($nfw->session->userdata('id_kontrak')!=''){
            $id_kontrak=$nfw->session->userdata('id_kontrak');
            $nfw->db->where('a.id_kontrak',$id_kontrak);
            $nfw->db->join('mitra b','a.id_mitra=b.id_mitra','left');
            $get=$nfw->db->get('kontrak a');
            if($get->num_rows()>0){
                $r=$get->row();
                if($a=='all'){
                    return $r;
                }
                else{
                    return $r->$a;
                }
            }
            else{
                return false;
            }
        }   
        else{
            return false;
        } 
    }
    function doc($id_kontrak,$tgl=array(),$num=''){
        $nfw=&get_instance();
        if(count($tgl)>0){
            $nfw->db->where("DATE_FORMAT(a.tgl_kirim,'".$tgl[0]."')<='".$tgl[1]."'");
        }
        $get=$nfw->db->select('SUM(jumlah) as jumlah')
                        ->from('kirim_item a')
                        ->join('item b','a.id_item=b.id_item','LEFT')
                        ->where(['id_kontrak'=>$id_kontrak,'jenis'=>'DOC'])
                        ->get()
                        ->row();
        if($num==''){
            return number_format($get->jumlah);
        }
        else{
            return $get->jumlah;
        }
    }
    function mati($id_kontrak,$tgl=array(),$num=''){
        $nfw=&get_instance();
        if(count($tgl)>0){
            $nfw->db->where("DATE_FORMAT(a.tgl_data,'".$tgl[0]."')<='".$tgl[1]."'");
        }
        $get=$nfw->db->select('SUM(mati) as jumlah')
                        ->from('ayam a')
                        ->where(['id_kontrak'=>$id_kontrak])
                        ->get()
                        ->row();
        if($num==''){
            return number_format($get->jumlah);
        }
        else{
            return $get->jumlah;
        }
    }
    function afkir($id_kontrak,$tgl=array(),$num=''){
        $nfw=&get_instance();
        if(count($tgl)>0){
            $nfw->db->where("DATE_FORMAT(a.tgl_data,'".$tgl[0]."')<='".$tgl[1]."'");
        }
        $get=$nfw->db->select('SUM(afkir) as jumlah')
                        ->from('ayam a')
                        ->where(['id_kontrak'=>$id_kontrak])
                        ->get()
                        ->row();
        if($num==''){
            return number_format($get->jumlah);
        }
        else{
            return $get->jumlah;
        }
    }
    function jual($id_kontrak,$tgl=array(),$num=''){
        $nfw=&get_instance();
        if(count($tgl)>0){
            $nfw->db->where("DATE_FORMAT(a.tgl_jual,'".$tgl[0]."')<='".$tgl[1]."'");
        }
        $get=$nfw->db->select('SUM(jumlah) as jumlah')
                        ->from('jual a')
                        ->where(['id_kontrak'=>$id_kontrak])
                        ->get()
                        ->row();
        if($num==''){
            return number_format($get->jumlah);
        }
        else{
            return $get->jumlah;
        }
    }
    function sisa($id_kontrak,$tgl=array()){
        $doc=doc($id_kontrak,$tgl,'1');
        $mati=(int)mati($id_kontrak,$tgl,'1');
        $afkir=(int)afkir($id_kontrak,$tgl,'1');
        $jual=(int)jual($id_kontrak,$tgl,'1');
        $hitung=($doc-($mati+$afkir+$jual));
        return number_format($hitung);
    }
    function sisaDataAyam($id_kontrak,$tgl=array(),$num=''){
        $doc=doc($id_kontrak,$tgl,'1');
        $mati=(int)mati($id_kontrak,$tgl,'1');
        $afkir=(int)afkir($id_kontrak,$tgl,'1');
        $hitung=($doc-($mati+$afkir));
        if($num==''){
            return number_format($hitung);
        }
        else{
            return $hitung;
        }
    }
    function genNoKontrak(){
        $nfw=&get_instance();
        $nfw->db->order_by('id_kontrak','DESC');
        $nfw->db->limit(1);
        $get=$nfw->db->get('kontrak');
        if($get->num_rows()>0){
            $r=$get->row();
            $no_kontrak=$r->no_kontrak;
            $no_kontrak=substr($no_kontrak,0,-8);
            $no_kontrak=substr($no_kontrak,4);
            $no_kontrak=$no_kontrak+1;
            $no_kontrak=(strlen($no_kontrak)==1)?'0'.$no_kontrak:$no_kontrak;
            return 'SPK-'.$no_kontrak.'/'.date('m').'/'.date('Y');

        }
        else{
            return 'SPK-01/'.date('m').'/'.date('Y');
        }
    }
    function getSisaAyam($id_kontrak=''){
        $nfw=&get_instance();
        if($id_kontrak!=''){
            $nfw->db->where('id_kontrak',$id_kontrak);
            $get=$nfw->db->select('SUM(jumlah) as jumlah')
                            ->from('mati')
                            ->get()->row();
            $mati=$get->jumlah;
            $nfw->db->where('id_kontrak',$id_kontrak);
            $get=$nfw->db->select('SUM(jumlah) as jumlah')
                            ->from('jual')
                            ->get()->row();
            $jual=$get->jumlah;
            $nfw->db->where('id_kontrak',$id_kontrak);
            $get=$nfw->db->select('SUM(jumlah) as jumlah')
                            ->from('kirim_item a')
                            ->join('item b','a.id_item=b.id_item','LEFT')
                            ->where('jenis','DOC')
                            ->get()->row();
            $doc=$get->jumlah;


            return $doc-($jual+$mati);

        }
        else{
            return false;
        }

    }
    function headingLaporan($bulan,$tahun){
        $heading='Rekapitulasi : ';
        if($tahun!=''){
            if($bulan==''){
                $heading.='Tahun '.$tahun;
            }
            else{
                $heading.=bulan_huruf($bulan).' '.$tahun;
            }
        }
        else{
            if($bulan==''){
                $heading='';
            }
            else{
                $heading.=bulan_huruf($bulan);
            }
        }
        return $heading;
    
    }
    
	function request_all($fillable){
		foreach ($fillable as $key => $value) {
	      if(isset($_POST[$value])){
	        $data[$value]=$_POST[$value];
	      }
	    }
		return $data;
	}