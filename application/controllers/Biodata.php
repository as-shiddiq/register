<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Biodata extends CI_Controller {

##################################
##          CONSTRUCT           ##
##################################

public function __construct()
{
	parent::__construct();
	$this->load->model('biodataModel');
}
##################################
##          VIEW DEFAULT        ##
##################################
public function index(){
	$get_data=$this->biodataModel->get_data();
	$template = array(
		'table_open' => '<table border="0" cellpadding="4" cellspacing="0" class="table table-bordered dt-responsive display" id="table-dt">',
	);
	$this->table->set_template($template);
	$this->table->set_heading('No','nama lengkap','TTL','jk','nama pekerjaan','kewarganegaraan','tmpt tinggal','agama','','');
	$i=1;
	foreach($get_data->result() as $row){
		$this->table->add_row(array("data"=>$i,"width"=>"50px","align"=>"center"),
													$row->nama_lengkap,
													$row->tmpt_lahir.' '.standar_tanggal($row->tgl_lahir),
													$row->jk,
													$row->nama_pekerjaan,
													$row->kewarganegaraan,
													$row->tmpt_tinggal,
													$row->agama,
													array("data"=>anchor(site_url("biodata?ubah&nik=".$row->nik),"<i class='fa fa-edit'></i> Ubah",["class"=>"btn btn-xs btn-info btn-sunting","onclick"=>false]),"width"=>"20px","align"=>"center"),
													array("data"=>anchor(site_url("biodata/hapus?nik=".$row->nik),"<i class='fa fa-trash'></i> Hapus",["class"=>"btn btn-xs btn-danger","onclick"=>"return confirm('Yakin Hapus Data?')"]),"width"=>"20px","align"=>"center"));
		$i++;
	}
	$databody['table']=$this->table->generate();
	$data['title']='Data Biodata';
	$data['body']=$this->load->view('biodataView',$databody,true);
	$data['js']=$this->load->view('js/biodataJs',$databody,true);
	$this->load->view('templated/html',$data);
}

##################################
##            SIMPAN            ##
##################################

public function simpan(){
	if($this->input->post('simpan')){
		$data=$this->_datapost();
		$this->biodataModel->insert($data);
	}
	redirect('biodata');
}
##################################
##            UBAH              ##
##################################

public function ubah(){
	if($this->input->post('simpan')){
		$data=$this->_datapost();
		$nik=$this->input->post('def_nik');
		$where=array(
			'nik'=>$nik
		);
		$this->biodataModel->update($data,$where);
	}
	redirect('Biodata');
}

##################################
##            HAPUS             ##
##################################

public function hapus(){
	$where=array(
			'nik'=>$this->input->get(nik)
		);
	$this->biodataModel->delete($where);
	redirect('biodata');
}
##################################
##      KUMPULKAN NILAI POST    ##
##################################

private function _datapost(){
	$nik = $this->input->post('nik');
	$id_pekerjaan = $this->input->post('id_pekerjaan');
	$id_pendidikan = $this->input->post('id_pendidikan');
	$nama_lengkap = $this->input->post('nama_lengkap');
	$tmpt_lahir = $this->input->post('tmpt_lahir');
	$tgl_lahir = $this->input->post('tgl_lahir');
	$jk = $this->input->post('jk');
	$kewarganegaraan = $this->input->post('kewarganegaraan');
	$tmpt_tinggal = $this->input->post('tmpt_tinggal');
	$agama = $this->input->post('agama');
	$created_at = $this->input->post('created_at');
	$data=array(
			'nik'=>$nik,
			'id_pekerjaan'=>$id_pekerjaan,
			'id_pendidikan'=>$id_pendidikan,
			'nama_lengkap'=>$nama_lengkap,
			'tmpt_lahir'=>$tmpt_lahir,
			'tgl_lahir'=>$tgl_lahir,
			'jk'=>$jk,
			'kewarganegaraan'=>$kewarganegaraan,
			'tmpt_tinggal'=>$tmpt_tinggal,
			'agama'=>$agama,
			'created_at'=>$created_at
		);
	return $data;
}
//end class
}
