<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User extends CI_Controller {

##################################
##          CONSTRUCT           ##
##################################

public function __construct()
{
	parent::__construct();
	$this->load->model('userModel');
}
##################################
##          VIEW DEFAULT        ##
##################################
public function index(){
	$get_data=$this->userModel->get_data();
	$template = array(
		'table_open' => '<table border="0" cellpadding="4" cellspacing="0" class="table table-bordered dt-responsive display" id="table-dt">',
	);
	$this->table->set_template($template);
	$this->table->set_heading('No','username','password','nickname','','');
	$i=1;
	foreach($get_data->result() as $row){
		$this->table->add_row(array("data"=>$i,"width"=>"50px","align"=>"center"),
													$row->username,
													$row->password,
													$row->nickname,
													array("data"=>anchor(site_url("user/sunting?id_user=".$row->id_user),"<i class='fa fa-edit'></i> Ubah",["class"=>"btn btn-xs btn-info btn-sunting","onclick"=>false]),"width"=>"20px","align"=>"center"),
													array("data"=>anchor(site_url("user/hapus?id_user=".$row->id_user),"<i class='fa fa-trash'></i> Hapus",["class"=>"btn btn-xs btn-danger","onclick"=>"return confirm('Yakin Hapus Data?')"]),"width"=>"20px","align"=>"center"));
		$i++;
	}
	$databody['table']=$this->table->generate();
	$data['title']='Pengguna';
	$data['body']=$this->load->view('userView',$databody,true);
	$data['js']=$this->load->view('js/userJs',$databody,true);
	$this->load->view('templated/html',$data);
}
##################################
##           SUNTING            ##
##################################

public function sunting(){
	$where=[
			'id_user'=>$_GET['id_user']
			];
	$row=$this->db->get_where('user',$where)->row_array();
	$response['data']=$row;
	header('Content-type: application/json;charset=utf-8');
	echo json_encode($response,JSON_PRETTY_PRINT);
}

##################################
##            SIMPAN            ##
##################################

public function simpan(){
	if($this->input->post('simpan')){
		$data=$this->_datapost();
		$this->userModel->insert($data);
	}
	redirect('user');
}
##################################
##            UBAH              ##
##################################

public function ubah(){
	if($this->input->post('simpan')){
		$data=$this->_datapost();
		$id_user=$this->input->post('id_user');
		$where=array(
			'id_user'=>$id_user
		);
		$this->userModel->update($data,$where);
	}
	redirect('User');
}

##################################
##            HAPUS             ##
##################################

public function hapus(){
	$where=array(
			'id_user'=>$this->input->get(id_user)
		);
	$this->userModel->delete($where);
	redirect('user');
}
##################################
##      KUMPULKAN NILAI POST    ##
##################################

private function _datapost(){
	$id_user = $this->input->post('id_user');
	$username = $this->input->post('username');
	$password = $this->input->post('password');
	$nickname = $this->input->post('nickname');
	$id_karyawan = $this->input->post('id_karyawan');
	$id_pemasok = $this->input->post('id_pemasok');
	$level = $this->input->post('level');
	$data=array(
			'id_user'=>$id_user,
			'username'=>$username,
			'password'=>$password,
			'nickname'=>$nickname,
			'id_karyawan'=>$id_karyawan,
			'id_pemasok'=>$id_pemasok,
			'level'=>$level,
		);
	return $data;
}
//end class
}
