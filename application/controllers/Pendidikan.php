<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pendidikan extends CI_Controller {

##################################
##          CONSTRUCT           ##
##################################

public function __construct()
{
	parent::__construct();
	$this->load->model('pendidikanModel');
}
##################################
##          VIEW DEFAULT        ##
##################################
public function index(){
	$get_data=$this->pendidikanModel->get_data();
	$template = array(
		'table_open' => '<table border="0" cellpadding="4" cellspacing="0" class="table table-bordered dt-responsive display" id="table-dt">',
	);
	$this->table->set_template($template);
	$this->table->set_heading('No','nama pendidikan','','');
	$i=1;
	foreach($get_data->result() as $row){
		$this->table->add_row(array("data"=>$i,"width"=>"50px","align"=>"center"),
													$row->nama_pendidikan,
													array("data"=>anchor(site_url("pendidikan/sunting?id_pendidikan=".$row->id_pendidikan),"<i class='fa fa-edit'></i> Ubah",["class"=>"btn btn-xs btn-info btn-sunting","onclick"=>false]),"width"=>"20px","align"=>"center"),
													array("data"=>anchor(site_url("pendidikan/hapus?id_pendidikan=".$row->id_pendidikan),"<i class='fa fa-trash'></i> Hapus",["class"=>"btn btn-xs btn-danger","onclick"=>"return confirm('Yakin Hapus Data?')"]),"width"=>"20px","align"=>"center"));
		$i++;
	}
	$databody['table']=$this->table->generate();
	$data['title']='Data Pendidikan';
	$data['body']=$this->load->view('pendidikanView',$databody,true);
	$data['js']=$this->load->view('js/pendidikanJs',$databody,true);
	$this->load->view('templated/html',$data);
}
##################################
##           SUNTING            ##
##################################

public function sunting(){
	$where=[
			'id_pendidikan'=>$_GET['id_pendidikan']
			];
	$row=$this->db->get_where('pendidikan',$where)->row_array();
	$response['data']=$row;
	header('Content-type: application/json;charset=utf-8');
	echo json_encode($response,JSON_PRETTY_PRINT);
}

##################################
##            SIMPAN            ##
##################################

public function simpan(){
	if($this->input->post('simpan')){
		$data=$this->_datapost();
		$this->pendidikanModel->insert($data);
	}
	redirect('pendidikan');
}
##################################
##            UBAH              ##
##################################

public function ubah(){
	if($this->input->post('simpan')){
		$data=$this->_datapost();
		$id_pendidikan=$this->input->post('id_pendidikan');
		$where=array(
			'id_pendidikan'=>$id_pendidikan
		);
		$this->pendidikanModel->update($data,$where);
	}
	redirect('Pendidikan');
}

##################################
##            HAPUS             ##
##################################

public function hapus(){
	$where=array(
			'id_pendidikan'=>$this->input->get(id_pendidikan)
		);
	$this->pendidikanModel->delete($where);
	redirect('pendidikan');
}
##################################
##      KUMPULKAN NILAI POST    ##
##################################

private function _datapost(){
	$id_pendidikan = $this->input->post('id_pendidikan');
	$nama_pendidikan = $this->input->post('nama_pendidikan');
	$data=array(
			'id_pendidikan'=>$id_pendidikan,
			'nama_pendidikan'=>$nama_pendidikan
		);
	return $data;
}
//end class
}
