<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Rb2 extends CI_Controller {

##################################
##          CONSTRUCT           ##
##################################

public function __construct()
{
	parent::__construct();
	$this->load->model('rb2Model');
}
##################################
##          VIEW DEFAULT        ##
##################################
public function index(){
	$get_data=$this->rb2Model->get_data();
	$template = array(
		'table_open' => '<table border="0" cellpadding="4" cellspacing="0" class="table table-bordered dt-responsive display" id="table-dt">',
	);
	$this->table->set_template($template);
	$this->table->set_heading('No','reg perkara','tgl penerimaan tgg jwb','jpu','nik','psl didakwakan','jml ukrn jns','penyimpanan','tgl penyerahan','no & tgl putusan pn','no & tgl putusan pt','no & tgl putusan ma','tgl ba plksanaan ptusan','no penguguman','tgl pengumuman','no ijin lelang','tgl ijin lelang','tgl ba penyerahan','no pemanfaatan','tgl pemanfaatan','tgl ba instansi','no pengumuman bt','tgl pengumuman bt','no izin lelang','tgl izin lelang','tgl ba penyerahan bt','no pemanfaatan bt','tgl pemanfaatan bt','keterangan','tgl simpan','','');
	$i=1;
	foreach($get_data->result() as $row){
		$this->table->add_row(array("data"=>$i,"width"=>"50px","align"=>"center"),
													$row->reg_perkara,
													$row->tgl_penerimaan_tgg_jwb,
													$row->jpu,
													$row->nik,
													$row->psl_didakwakan,
													$row->jml_ukrn_jns,
													$row->penyimpanan,
													$row->tgl_penyerahan,
													$row->no_putusan_pn.'<br>'.$row->tgl_putusan_pn,
													$row->no_putusan_pt.'<br>'.$row->tgl_putusan_pt,
													$row->no_putusan_ma.'<br>'.$row->tgl_putusan_ma,
													$row->tgl_ba_plksanaan_ptusan,
													$row->no_penguguman,
													$row->tgl_pengumuman,
													$row->no_ijin_lelang,
													$row->tgl_ijin_lelang,
													$row->tgl_ba_penyerahan,
													$row->no_pemanfaatan,
													$row->tgl_pemanfaatan,
													$row->tgl_ba_instansi,
													$row->no_pengumuman_bt,
													$row->tgl_pengumuman_bt,
													$row->no_izin_lelang,
													$row->tgl_izin_lelang,
													$row->tgl_ba_penyerahan_bt,
													$row->no_pemanfaatan_bt,
													$row->tgl_pemanfaatan_bt,
													$row->keterangan,
													$row->tgl_simpan,
													array("data"=>anchor(site_url("rb2?ubah&no_rb2=".$row->no_rb2),"<i class='fa fa-edit'></i> Ubah",["class"=>"btn btn-xs btn-info btn-sunting","onclick"=>false]),"width"=>"20px","align"=>"center"),
													array("data"=>anchor(site_url("rb2/hapus?no_rb2=".$row->no_rb2),"<i class='fa fa-trash'></i> Hapus",["class"=>"btn btn-xs btn-danger","onclick"=>"return confirm('Yakin Hapus Data?')"]),"width"=>"20px","align"=>"center"));
		$i++;
	}
	$databody['table']=$this->table->generate();
	$data['title']=rbConvert(1,0).'-'.rbConvert(1,1);
	$data['body']=$this->load->view('rb2View',$databody,true);
	$data['js']=$this->load->view('js/rb2Js',$databody,true);
	$this->load->view('templated/html',$data);
}

##################################
##            SIMPAN            ##
##################################

public function simpan(){
	if($this->input->post('simpan')){
		$data=$this->_datapost();
		$this->rb2Model->insert($data);
	}
	redirect('rb2');
}
##################################
##            UBAH              ##
##################################

public function ubah(){
	if($this->input->post('simpan')){
		$data=$this->_datapost();
		$no_rb2=$this->input->post('no_rb2');
		$where=array(
			'no_rb2'=>$no_rb2
		);
		$this->rb2Model->update($data,$where);
	}
	redirect('Rb2');
}

##################################
##            HAPUS             ##
##################################

public function hapus(){
	$where=array(
			'no_rb2'=>$this->input->get(no_rb2)
		);
	$this->rb2Model->delete($where);
	redirect('rb2');
}
##################################
##      KUMPULKAN NILAI POST    ##
##################################

private function _datapost(){
	$no_rb2 = $this->input->post('no_rb2');
	$reg_perkara = $this->input->post('reg_perkara');
	$tgl_penerimaan_tgg_jwb = $this->input->post('tgl_penerimaan_tgg_jwb');
	$jpu = $this->input->post('jpu');
	$nik = $this->input->post('nik');
	$psl_didakwakan = $this->input->post('psl_didakwakan');
	$jml_ukrn_jns = $this->input->post('jml_ukrn_jns');
	$penyimpanan = $this->input->post('penyimpanan');
	$tgl_penyerahan = $this->input->post('tgl_penyerahan');
	$no_putusan_pn = $this->input->post('no_putusan_pn');
	$tgl_putusan_pn = $this->input->post('tgl_putusan_pn');
	$no_putusan_pt = $this->input->post('no_putusan_pt');
	$tgl_putusan_pt = $this->input->post('tgl_putusan_pt');
	$no_putusan_ma = $this->input->post('no_putusan_ma');
	$tgl_putusan_ma = $this->input->post('tgl_putusan_ma');
	$tgl_ba_plksanaan_ptusan = $this->input->post('tgl_ba_plksanaan_ptusan');
	$no_penguguman = $this->input->post('no_penguguman');
	$tgl_pengumuman = $this->input->post('tgl_pengumuman');
	$no_ijin_lelang = $this->input->post('no_ijin_lelang');
	$tgl_ijin_lelang = $this->input->post('tgl_ijin_lelang');
	$tgl_ba_penyerahan = $this->input->post('tgl_ba_penyerahan');
	$no_pemanfaatan = $this->input->post('no_pemanfaatan');
	$tgl_pemanfaatan = $this->input->post('tgl_pemanfaatan');
	$tgl_ba_instansi = $this->input->post('tgl_ba_instansi');
	$no_pengumuman_bt = $this->input->post('no_pengumuman_bt');
	$tgl_pengumuman_bt = $this->input->post('tgl_pengumuman_bt');
	$no_izin_lelang = $this->input->post('no_izin_lelang');
	$tgl_izin_lelang = $this->input->post('tgl_izin_lelang');
	$tgl_ba_penyerahan_bt = $this->input->post('tgl_ba_penyerahan_bt');
	$no_pemanfaatan_bt = $this->input->post('no_pemanfaatan_bt');
	$tgl_pemanfaatan_bt = $this->input->post('tgl_pemanfaatan_bt');
	$keterangan = $this->input->post('keterangan');
	$tgl_simpan = $this->input->post('tgl_simpan');
	$data=array(
			'no_rb2'=>$no_rb2,
			'reg_perkara'=>$reg_perkara,
			'tgl_penerimaan_tgg_jwb'=>$tgl_penerimaan_tgg_jwb,
			'jpu'=>$jpu,
			'nik'=>$nik,
			'psl_didakwakan'=>$psl_didakwakan,
			'jml_ukrn_jns'=>$jml_ukrn_jns,
			'penyimpanan'=>$penyimpanan,
			'tgl_penyerahan'=>$tgl_penyerahan,
			'no_putusan_pn'=>$no_putusan_pn,
			'tgl_putusan_pn'=>$tgl_putusan_pn,
			'no_putusan_pt'=>$no_putusan_pt,
			'tgl_putusan_pt'=>$tgl_putusan_pt,
			'no_putusan_ma'=>$no_putusan_ma,
			'tgl_putusan_ma'=>$tgl_putusan_ma,
			'tgl_ba_plksanaan_ptusan'=>$tgl_ba_plksanaan_ptusan,
			'no_penguguman'=>$no_penguguman,
			'tgl_pengumuman'=>$tgl_pengumuman,
			'no_ijin_lelang'=>$no_ijin_lelang,
			'tgl_ijin_lelang'=>$tgl_ijin_lelang,
			'tgl_ba_penyerahan'=>$tgl_ba_penyerahan,
			'no_pemanfaatan'=>$no_pemanfaatan,
			'tgl_pemanfaatan'=>$tgl_pemanfaatan,
			'tgl_ba_instansi'=>$tgl_ba_instansi,
			'no_pengumuman_bt'=>$no_pengumuman_bt,
			'tgl_pengumuman_bt'=>$tgl_pengumuman_bt,
			'no_izin_lelang'=>$no_izin_lelang,
			'tgl_izin_lelang'=>$tgl_izin_lelang,
			'tgl_ba_penyerahan_bt'=>$tgl_ba_penyerahan_bt,
			'no_pemanfaatan_bt'=>$no_pemanfaatan_bt,
			'tgl_pemanfaatan_bt'=>$tgl_pemanfaatan_bt,
			'keterangan'=>$keterangan,
			'tgl_simpan'=>$tgl_simpan
		);
	return $data;
}
//end class
}
