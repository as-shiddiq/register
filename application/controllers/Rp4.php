<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Rp4 extends CI_Controller {

##################################
##          CONSTRUCT           ##
##################################

public function __construct()
{
	parent::__construct();
	$this->load->model('rp4Model');
}
##################################
##          VIEW DEFAULT        ##
##################################
public function index(){
	$get_data=$this->rp4Model->get_data();
	$template = array(
		'table_open' => '<table border="0" cellpadding="4" cellspacing="0" class="table table-bordered dt-responsive display" id="table-dt">',
	);
	$this->table->set_template($template);
	$this->table->set_heading('No','tgl','alamat dipanggil','alasan pemanggilan','tgl reg perkara','tmpt/Tgl/jam menghadap','menghadap kpd','keterangan','','');
	$i=1;
	foreach($get_data->result() as $row){
		$this->table->add_row(array("data"=>$i,"width"=>"50px","align"=>"center"),
													$row->tgl,
													$row->alamat_dipanggil,
													$row->alasan_pemanggilan,
													$row->tgl_reg_perkara,
													$row->tmpt_menghadap.' '.$row->tgl_menghadap.''.$row->jam_menghadap,
													$row->menghadap_kpd,
													$row->keterangan,
													array("data"=>anchor(site_url("rp4?ubah&no_rp4=".$row->no_rp4),"<i class='fa fa-edit'></i> Ubah",["class"=>"btn btn-xs btn-info btn-sunting","onclick"=>false]),"width"=>"20px","align"=>"center"),
													array("data"=>anchor(site_url("rp4/hapus?no_rp4=".$row->no_rp4),"<i class='fa fa-trash'></i> Hapus",["class"=>"btn btn-xs btn-danger","onclick"=>"return confirm('Yakin Hapus Data?')"]),"width"=>"20px","align"=>"center"));
		$i++;
	}
	$databody['table']=$this->table->generate();
	$data['title']=rpConvert(3,0).'-'.rpConvert(3,1);
	$data['body']=$this->load->view('rp4View',$databody,true);
	$data['js']=$this->load->view('js/rp4Js',$databody,true);
	$this->load->view('templated/html',$data);
}

##################################
##            SIMPAN            ##
##################################

public function simpan(){
	if($this->input->post('simpan')){
		$data=$this->_datapost();
		$this->rp4Model->insert($data);
	}
	redirect('rp4');
}
##################################
##            UBAH              ##
##################################

public function ubah(){
	if($this->input->post('simpan')){
		$data=$this->_datapost();
		$no_rp4=$this->input->post('no_rp4');
		$where=array(
			'no_rp4'=>$no_rp4
		);
		$this->rp4Model->update($data,$where);
	}
	redirect('Rp4');
}

##################################
##            HAPUS             ##
##################################

public function hapus(){
	$where=array(
			'no_rp4'=>$this->input->get(no_rp4)
		);
	$this->rp4Model->delete($where);
	redirect('rp4');
}
##################################
##      KUMPULKAN NILAI POST    ##
##################################

private function _datapost(){
	$no_rp4 = $this->input->post('no_rp4');
	$tgl = $this->input->post('tgl');
	$alamat_dipanggil = $this->input->post('alamat_dipanggil');
	$alasan_pemanggilan = $this->input->post('alasan_pemanggilan');
	$no_rp3 = $this->input->post('no_rp3');
	$tmpt_menghadap = $this->input->post('tmpt_menghadap');
	$tgl_menghadap = $this->input->post('tgl_menghadap');
	$jam_menghadap = $this->input->post('jam_menghadap');
	$menghadap_kpd = $this->input->post('menghadap_kpd');
	$keterangan = $this->input->post('keterangan');
	$tgl_simpan = $this->input->post('tgl_simpan');
	$no_agenda = $this->input->post('no_agenda');
	$data=array(
			'no_rp4'=>$no_rp4,
			'tgl'=>$tgl,
			'alamat_dipanggil'=>$alamat_dipanggil,
			'alasan_pemanggilan'=>$alasan_pemanggilan,
			'no_rp3'=>$no_rp3,
			'tmpt_menghadap'=>$tmpt_menghadap,
			'tgl_menghadap'=>$tgl_menghadap,
			'jam_menghadap'=>$jam_menghadap,
			'menghadap_kpd'=>$menghadap_kpd,
			'keterangan'=>$keterangan,
			'tgl_simpan'=>$tgl_simpan,
			'no_agenda'=>$no_agenda
		);
	return $data;
}
//end class
}
