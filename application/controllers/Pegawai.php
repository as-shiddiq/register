<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pegawai extends CI_Controller {

##################################
##          CONSTRUCT           ##
##################################

public function __construct()
{
	parent::__construct();
	$this->load->model('pegawaiModel');
}
##################################
##          VIEW DEFAULT        ##
##################################
public function index(){
	$get_data=$this->pegawaiModel->get_data();
	$template = array(
		'table_open' => '<table border="0" cellpadding="4" cellspacing="0" class="table table-bordered dt-responsive display" id="table-dt">',
	);
	$this->table->set_template($template);
	$this->table->set_heading('No','nip','nama pegawai','gol','','');
	$i=1;
	foreach($get_data->result() as $row){
		$this->table->add_row(array("data"=>$i,"width"=>"50px","align"=>"center"),
													$row->nip,
													$row->nama_pegawai,
													$row->gol.' '.$row->pangkat,
													array("data"=>anchor(site_url("pegawai/sunting?id_pegawai=".$row->id_pegawai),"<i class='fa fa-edit'></i> Ubah",["class"=>"btn btn-xs btn-info btn-sunting","onclick"=>false]),"width"=>"20px","align"=>"center"),
													array("data"=>anchor(site_url("pegawai/hapus?id_pegawai=".$row->id_pegawai),"<i class='fa fa-trash'></i> Hapus",["class"=>"btn btn-xs btn-danger","onclick"=>"return confirm('Yakin Hapus Data?')"]),"width"=>"20px","align"=>"center"));
		$i++;
	}
	$databody['table']=$this->table->generate();
	$data['title']='Data Pegawai';
	$data['body']=$this->load->view('pegawaiView',$databody,true);
	$data['js']=$this->load->view('js/pegawaiJs',$databody,true);
	$this->load->view('templated/html',$data);
}
##################################
##           SUNTING            ##
##################################

public function sunting(){
	$where=[
			'id_pegawai'=>$_GET['id_pegawai']
			];
	$row=$this->db->get_where('pegawai',$where)->row_array();
	$response['data']=$row;
	header('Content-type: application/json;charset=utf-8');
	echo json_encode($response,JSON_PRETTY_PRINT);
}

##################################
##            SIMPAN            ##
##################################

public function simpan(){
	if($this->input->post('simpan')){
		$data=$this->_datapost();
		$this->pegawaiModel->insert($data);
	}
	redirect('pegawai');
}
##################################
##            UBAH              ##
##################################

public function ubah(){
	if($this->input->post('simpan')){
		$data=$this->_datapost();
		$id_pegawai=$this->input->post('id_pegawai');
		$where=array(
			'id_pegawai'=>$id_pegawai
		);
		$this->pegawaiModel->update($data,$where);
	}
	redirect('Pegawai');
}

##################################
##            HAPUS             ##
##################################

public function hapus(){
	$where=array(
			'id_pegawai'=>$this->input->get(id_pegawai)
		);
	$this->pegawaiModel->delete($where);
	redirect('pegawai');
}
##################################
##      KUMPULKAN NILAI POST    ##
##################################

private function _datapost(){
	$id_pegawai = $this->input->post('id_pegawai');
	$nip = $this->input->post('nip');
	$nama_pegawai = $this->input->post('nama_pegawai');
	$id_pangkat_gol = $this->input->post('id_pangkat_gol');
	$data=array(
			'id_pegawai'=>$id_pegawai,
			'nip'=>$nip,
			'nama_pegawai'=>$nama_pegawai,
			'id_pangkat_gol'=>$id_pangkat_gol
		);
	return $data;
}
//end class
}
