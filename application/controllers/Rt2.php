<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Rt2 extends CI_Controller {

##################################
##          CONSTRUCT           ##
##################################

public function __construct()
{
	parent::__construct();
	$this->load->model('rt2Model');
}
##################################
##          VIEW DEFAULT        ##
##################################
public function index(){
	$get_data=$this->rt2Model->get_data();
	$template = array(
		'table_open' => '<table border="0" cellpadding="4" cellspacing="0" class="table table-bordered dt-responsive display" id="table-dt">',
	);
	$this->table->set_template($template);
	$this->table->set_heading('No','no perpanjangan','tgl ketetapan perpanjangan','nama lengkap','pasal yg disangkakan','nomor','tgl','tgl ditahan','tgl perpanjangan','tgl perpanjangan2','ditangguhkan','keterangan','tgl simpan','','');
	$i=1;
	foreach($get_data->result() as $row){
		$this->table->add_row(array("data"=>$i,"width"=>"50px","align"=>"center"),
													$row->no_perpanjangan,
													$row->tgl_ketetapan_perpanjangan,
													$row->nama_lengkap,
													$row->pasal_yg_disangkakan,
													$row->nomor,
													$row->tgl,
													$row->tgl_ditahan,
													$row->tgl_perpanjangan,
													$row->tgl_perpanjangan2,
													$row->ditangguhkan,
													$row->keterangan,
													$row->tgl_simpan,
													array("data"=>anchor(site_url("rt2?ubah&no_rt2=".$row->no_rt2),"<i class='fa fa-edit'></i> Ubah",["class"=>"btn btn-xs btn-info btn-sunting","onclick"=>false]),"width"=>"20px","align"=>"center"),
													array("data"=>anchor(site_url("rt2/hapus?no_rt2=".$row->no_rt2),"<i class='fa fa-trash'></i> Hapus",["class"=>"btn btn-xs btn-danger","onclick"=>"return confirm('Yakin Hapus Data?')"]),"width"=>"20px","align"=>"center"));
		$i++;
	}
	$databody['table']=$this->table->generate();
	$data['title']=rtConvert(2,0).'-'.rtConvert(2,1);
	$data['body']=$this->load->view('rt2View',$databody,true);
	$data['js']=$this->load->view('js/rt2Js',$databody,true);
	$this->load->view('templated/html',$data);
}

##################################
##            SIMPAN            ##
##################################

public function simpan(){
	if($this->input->post('simpan')){
		$data=$this->_datapost();
		$this->rt2Model->insert($data);
	}
	redirect('rt2');
}
##################################
##            UBAH              ##
##################################

public function ubah(){
	if($this->input->post('simpan')){
		$data=$this->_datapost();
		$no_rt2=$this->input->post('no_rt2');
		$where=array(
			'no_rt2'=>$no_rt2
		);
		$this->rt2Model->update($data,$where);
	}
	redirect('Rt2');
}

##################################
##            HAPUS             ##
##################################

public function hapus(){
	$where=array(
			'no_rt2'=>$this->input->get(no_rt2)
		);
	$this->rt2Model->delete($where);
	redirect('rt2');
}
##################################
##      KUMPULKAN NILAI POST    ##
##################################

private function _datapost(){
	$no_rt2 = $this->input->post('no_rt2');
	$no_perpanjangan = $this->input->post('no_perpanjangan');
	$tgl_ketetapan_perpanjangan = $this->input->post('tgl_ketetapan_perpanjangan');
	$nik = $this->input->post('nik');
	$pasal_yg_disangkakan = $this->input->post('pasal_yg_disangkakan');
	$nomor = $this->input->post('nomor');
	$tgl = $this->input->post('tgl');
	$tgl_ditahan = $this->input->post('tgl_ditahan');
	$tgl_perpanjangan = $this->input->post('tgl_perpanjangan');
	$tgl_perpanjangan2 = $this->input->post('tgl_perpanjangan2');
	$ditangguhkan = $this->input->post('ditangguhkan');
	$keterangan = $this->input->post('keterangan');
	$tgl_simpan = $this->input->post('tgl_simpan');
	$data=array(
			'no_rt2'=>$no_rt2,
			'no_perpanjangan'=>$no_perpanjangan,
			'tgl_ketetapan_perpanjangan'=>$tgl_ketetapan_perpanjangan,
			'nik'=>$nik,
			'pasal_yg_disangkakan'=>$pasal_yg_disangkakan,
			'nomor'=>$nomor,
			'tgl'=>$tgl,
			'tgl_ditahan'=>$tgl_ditahan,
			'tgl_perpanjangan'=>$tgl_perpanjangan,
			'tgl_perpanjangan2'=>$tgl_perpanjangan2,
			'ditangguhkan'=>$ditangguhkan,
			'keterangan'=>$keterangan,
			'tgl_simpan'=>$tgl_simpan
		);
	return $data;
}
//end class
}
