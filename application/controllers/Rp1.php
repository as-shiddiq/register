<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Rp1 extends CI_Controller {

##################################
##          CONSTRUCT           ##
##################################

public function __construct()
{
	parent::__construct();
	$this->load->model('rp1Model');
}
##################################
##          VIEW DEFAULT        ##
##################################
public function index(){
	$get_data=$this->rp1Model->get_data();
	$template = array(
		'table_open' => '<table border="0" cellpadding="4" cellspacing="0" class="table table-bordered dt-responsive display" id="table-dt">',
	);
	$this->table->set_template($template);
	$this->table->set_heading('No','nama lengkap','nama penerima','tgl penerimaan laporan','isi ringkas','diteruskan','keterangan','tgl simpan','no agenda','','');
	$i=1;
	foreach($get_data->result() as $row){
		$this->table->add_row(array("data"=>$i,"width"=>"50px","align"=>"center"),
													$row->nama_lengkap,
													$row->nama_penerima,
													$row->tgl_penerimaan_laporan,
													$row->isi_ringkas,
													$row->diteruskan,
													$row->keterangan,
													$row->tgl_simpan,
													$row->no_agenda,
													array("data"=>anchor(site_url("rp1?ubah&no_rp1=".$row->no_rp1),"<i class='fa fa-edit'></i> Ubah",["class"=>"btn btn-xs btn-info btn-sunting","onclick"=>false]),"width"=>"20px","align"=>"center"),
													array("data"=>anchor(site_url("rp1/hapus?no_rp1=".$row->no_rp1),"<i class='fa fa-trash'></i> Hapus",["class"=>"btn btn-xs btn-danger","onclick"=>"return confirm('Yakin Hapus Data?')"]),"width"=>"20px","align"=>"center"));
		$i++;
	}
	$databody['table']=$this->table->generate();
	$data['title']=rpConvert(1,0).'-'.rpConvert(1,1);
	$data['body']=$this->load->view('rp1View',$databody,true);
	$data['js']=$this->load->view('js/rp1Js',$databody,true);
	$this->load->view('templated/html',$data);
}

##################################
##            SIMPAN            ##
##################################

public function simpan(){
	if($this->input->post('simpan')){
		$data=$this->_datapost();
		$this->rp1Model->insert($data);
	}
	redirect('rp1');
}
##################################
##            UBAH              ##
##################################

public function ubah(){
	if($this->input->post('simpan')){
		$data=$this->_datapost();
		$no_rp1=$this->input->post('def_no_rp1');
		$where=array(
			'no_rp1'=>$no_rp1
		);
		$this->rp1Model->update($data,$where);
	}
	redirect('Rp1');
}

##################################
##            HAPUS             ##
##################################

public function hapus(){
	$where=array(
			'no_rp1'=>$this->input->get(no_rp1)
		);
	$this->rp1Model->delete($where);
	redirect('rp1');
}
##################################
##      KUMPULKAN NILAI POST    ##
##################################

private function _datapost(){
	$no_rp1 = $this->input->post('no_rp1');
	$nik = $this->input->post('nik');
	$nama_penerima = $this->input->post('nama_penerima');
	$tgl_penerimaan_laporan = $this->input->post('tgl_penerimaan_laporan');
	$isi_ringkas = $this->input->post('isi_ringkas');
	$diteruskan = $this->input->post('diteruskan');
	$keterangan = $this->input->post('keterangan');
	$tgl_simpan = $this->input->post('tgl_simpan');
	$no_agenda = $this->input->post('no_agenda');
	$data=array(
			'no_rp1'=>$no_rp1,
			'nik'=>$nik,
			'nama_penerima'=>$nama_penerima,
			'tgl_penerimaan_laporan'=>$tgl_penerimaan_laporan,
			'isi_ringkas'=>$isi_ringkas,
			'diteruskan'=>$diteruskan,
			'keterangan'=>$keterangan,
			'tgl_simpan'=>$tgl_simpan,
			'no_agenda'=>$no_agenda
		);
	return $data;
}
//end class
}
