<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Rp2 extends CI_Controller {

##################################
##          CONSTRUCT           ##
##################################

public function __construct()
{
	parent::__construct();
	$this->load->model('rp2Model');
}
##################################
##          VIEW DEFAULT        ##
##################################
public function index(){
	$get_data=$this->rp2Model->get_data();
	$template = array(
		'table_open' => '<table border="0" cellpadding="4" cellspacing="0" class="table table-bordered dt-responsive display" id="table-dt">',
	);
	$this->table->set_template($template);
	$this->table->set_heading('No','tgl penyelidikan','kasus','pegawai','nama pelapor','identitas','tgl ditutup','tgl ditingkatkan','keterangan','tgl simpan','no agenda','','');
	$i=1;
	foreach($get_data->result() as $row){
		$this->table->add_row(array("data"=>$i,"width"=>"50px","align"=>"center"),
													$row->tgl_penyelidikan,
													$row->kasus,
													$row->pegawai,
													$row->nama_pelapor,
													$row->identitas,
													$row->tgl_ditutup,
													$row->tgl_ditingkatkan,
													$row->keterangan,
													$row->tgl_simpan,
													$row->no_agenda,
													array("data"=>anchor(site_url("rp2?ubah&no_rp2=".$row->no_rp2),"<i class='fa fa-edit'></i> Ubah",["class"=>"btn btn-xs btn-info btn-sunting","onclick"=>false]),"width"=>"20px","align"=>"center"),
													array("data"=>anchor(site_url("rp2/hapus?no_rp2=".$row->no_rp2),"<i class='fa fa-trash'></i> Hapus",["class"=>"btn btn-xs btn-danger","onclick"=>"return confirm('Yakin Hapus Data?')"]),"width"=>"20px","align"=>"center"));
		$i++;
	}
	$databody['table']=$this->table->generate();
	$data['title']=rpConvert(2,0).'-'.rpConvert(2,1);
	$data['body']=$this->load->view('rp2View',$databody,true);
	$data['js']=$this->load->view('js/rp2Js',$databody,true);
	$this->load->view('templated/html',$data);
}

##################################
##            SIMPAN            ##
##################################

public function simpan(){
	if($this->input->post('simpan')){
		$data=$this->_datapost();
		$this->rp2Model->insert($data);
	}
	redirect('rp2');
}
##################################
##            UBAH              ##
##################################

public function ubah(){
	if($this->input->post('simpan')){
		$data=$this->_datapost();
		$no_rp2=$this->input->post('no_rp2');
		$where=array(
			'no_rp2'=>$no_rp2
		);
		$this->rp2Model->update($data,$where);
	}
	redirect('Rp2');
}

##################################
##            HAPUS             ##
##################################

public function hapus(){
	$where=array(
			'no_rp2'=>$this->input->get(no_rp2)
		);
	$this->rp2Model->delete($where);
	redirect('rp2');
}
##################################
##      KUMPULKAN NILAI POST    ##
##################################

private function _datapost(){
	$no_rp2 = $this->input->post('no_rp2');
	$tgl_penyelidikan = $this->input->post('tgl_penyelidikan');
	$kasus = $this->input->post('kasus');
	$pegawai = $this->input->post('pegawai');
	$nama_pelapor = $this->input->post('nama_pelapor');
	$identitas = $this->input->post('identitas');
	$tgl_ditutup = $this->input->post('tgl_ditutup');
	$tgl_ditingkatkan = $this->input->post('tgl_ditingkatkan');
	$keterangan = $this->input->post('keterangan');
	$tgl_simpan = $this->input->post('tgl_simpan');
	$no_agenda = $this->input->post('no_agenda');
	$data=array(
			'no_rp2'=>$no_rp2,
			'tgl_penyelidikan'=>$tgl_penyelidikan,
			'kasus'=>$kasus,
			'pegawai'=>$pegawai,
			'nama_pelapor'=>$nama_pelapor,
			'identitas'=>$identitas,
			'tgl_ditutup'=>$tgl_ditutup,
			'tgl_ditingkatkan'=>$tgl_ditingkatkan,
			'keterangan'=>$keterangan,
			'tgl_simpan'=>$tgl_simpan,
			'no_agenda'=>$no_agenda
		);
	return $data;
}
//end class
}
