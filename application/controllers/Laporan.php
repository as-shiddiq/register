<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Laporan extends CI_Controller {

##################################
##          CONSTRUCT           ##
##################################

public function __construct()
{
	parent::__construct();

}
##################################
##          VIEW DEFAULT        ##
##################################
public function index(){
	$databody['title']='Laporan';
	$data['js']=$this->load->view('js/LaporanJs',$databody,true);
	$data['body']=$this->load->view('LaporanView',$databody,true);
	$this->load->view('templated/html',$data);
}
public function cetak(){
		$jenis=$this->input->get('jenis');
		$bulan=isset($_GET['bulan'])?$_GET['bulan']:date('m');
		$tahun=isset($_GET['tahun'])?$_GET['tahun']:date('Y');

		$pdf=$this->load->view('cetak/'.$jenis.'Laporan','',TRUE);
		$namafile=$jenis.$bulan.$tahun;

		// echo $pdf;
		generate_pdf($pdf,$namafile,'legal','landscape');
	}
}
