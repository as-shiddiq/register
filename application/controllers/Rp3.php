<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Rp3 extends CI_Controller {

##################################
##          CONSTRUCT           ##
##################################

public function __construct()
{
	parent::__construct();
	$this->load->model('rp3Model');
}
##################################
##          VIEW DEFAULT        ##
##################################
public function index(){
	$get_data=$this->rp3Model->get_data();
	$template = array(
		'table_open' => '<table border="0" cellpadding="4" cellspacing="0" class="table table-bordered dt-responsive display" id="table-dt">',
	);
	$this->table->set_template($template);
	$this->table->set_heading('No','tgl reg perkara','asal perkara','identitas tsk','kasus','status tsk','no & tgl sp penahanan','no & tgl denda sitaan','jaksa','no & tgl penghentian DIK','pengalihan','no & tgl pelimpahan','keterangan','','');
	$i=1;
	foreach($get_data->result() as $row){
		$this->table->add_row(array("data"=>$i,"width"=>"50px","align"=>"center"),
													$row->tgl_reg_perkara,
													$row->asal_perkara,
													$row->nama_lengkap,
													$row->kasus,
													$row->status_tsk,
													$row->no_sp_penahanan.' '.$row->tgl_sp_penahanan,
													$row->no_denda_sitaan.' '.$row->tgl_denda_sitaan,
													$row->nama_pegawai,
													$row->no_penghentian_dik.' '.$row->tgl_penghentian_dik,
													$row->pengalihan,
													$row->no_pelimpahan.' '.$row->tgl_pelimpahan,
													$row->keterangan,
													array("data"=>anchor(site_url("rp3?ubah&no_rp3=".$row->no_rp3),"<i class='fa fa-edit'></i> Ubah",["class"=>"btn btn-xs btn-info btn-sunting","onclick"=>false]),"width"=>"20px","align"=>"center"),
													array("data"=>anchor(site_url("rp3/hapus?no_rp3=".$row->no_rp3),"<i class='fa fa-trash'></i> Hapus",["class"=>"btn btn-xs btn-danger","onclick"=>"return confirm('Yakin Hapus Data?')"]),"width"=>"20px","align"=>"center"));
		$i++;
	}
	$databody['table']=$this->table->generate();
	$data['title']=rpConvert(3,0).'-'.rpConvert(3,1);
	$data['body']=$this->load->view('rp3View',$databody,true);
	$data['js']=$this->load->view('js/rp3Js',$databody,true);
	$this->load->view('templated/html',$data);
}

##################################
##            SIMPAN            ##
##################################

public function simpan(){
	if($this->input->post('simpan')){
		$data=$this->_datapost();
		$this->rp3Model->insert($data);
	}
	redirect('rp3');
}
##################################
##            UBAH              ##
##################################

public function ubah(){
	if($this->input->post('simpan')){
		$data=$this->_datapost();
		$no_rp3=$this->input->post('no_rp3');
		$where=array(
			'no_rp3'=>$no_rp3
		);
		$this->rp3Model->update($data,$where);
	}
	redirect('Rp3');
}

##################################
##            HAPUS             ##
##################################

public function hapus(){
	$where=array(
			'no_rp3'=>$this->input->get(no_rp3)
		);
	$this->rp3Model->delete($where);
	redirect('rp3');
}
##################################
##      KUMPULKAN NILAI POST    ##
##################################

private function _datapost(){
	$no_rp3 = $this->input->post('no_rp3');
	$tgl_reg_perkara = $this->input->post('tgl_reg_perkara');
	$asal_perkara = $this->input->post('asal_perkara');
	$nik = $this->input->post('nik');
	$kasus = $this->input->post('kasus');
	$status_tsk = $this->input->post('status_tsk');
	$no_sp_penahanan = $this->input->post('no_sp_penahanan');
	$tgl_sp_penahanan = $this->input->post('tgl_sp_penahanan');
	$no_denda_sitaan = $this->input->post('no_denda_sitaan');
	$tgl_denda_sitaan = $this->input->post('tgl_denda_sitaan');
	$id_pegawai = $this->input->post('id_pegawai');
	$no_penghentian_dik = $this->input->post('no_penghentian_dik');
	$keterangan = $this->input->post('keterangan');
	$tgl_simpan = $this->input->post('tgl_simpan');
	$no_agenda = $this->input->post('no_agenda');
	$tgl_penghentian_dik = $this->input->post('tgl_penghentian_dik');
	$pengalihan = $this->input->post('pengalihan');
	$no_pelimpahan = $this->input->post('no_pelimpahan');
	$tgl_pelimpahan = $this->input->post('tgl_pelimpahan');
	$data=array(
			'no_rp3'=>$no_rp3,
			'tgl_reg_perkara'=>$tgl_reg_perkara,
			'asal_perkara'=>$asal_perkara,
			'nik'=>$nik,
			'kasus'=>$kasus,
			'status_tsk'=>$status_tsk,
			'no_sp_penahanan'=>$no_sp_penahanan,
			'tgl_sp_penahanan'=>$tgl_sp_penahanan,
			'no_denda_sitaan'=>$no_denda_sitaan,
			'tgl_denda_sitaan'=>$tgl_denda_sitaan,
			'id_pegawai'=>$id_pegawai,
			'no_penghentian_dik'=>$no_penghentian_dik,
			'keterangan'=>$keterangan,
			'tgl_simpan'=>$tgl_simpan,
			'no_agenda'=>$no_agenda,
			'tgl_penghentian_dik'=>$tgl_penghentian_dik,
			'pengalihan'=>$pengalihan,
			'no_pelimpahan'=>$no_pelimpahan,
			'tgl_pelimpahan'=>$tgl_pelimpahan
		);
	return $data;
}
//end class
}
