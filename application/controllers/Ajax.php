<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ajax extends CI_Controller {

    function getHargaKontrak($id_item,$id_kontrak){
        $get=$this->db->get_where('kon_item',['id_item'=>$id_item,'id_kontrak'=>$id_kontrak]);
        $harga_kontrak='Belum ditentukan';
        if($get->num_rows()>0){
            $row=$get->row();
            $harga_kontrak=$row->harga_kontrak;
        }
        $data['harga_kontrak']=$harga_kontrak;
        $response['data']=$data;
        echo json_encode($response);
    }
    function getHargaKontrakBw($av_bw,$id_kontrak){ 
        $get=$this->db->query('SELECT harga FROM kon_bw WHERE id_kontrak='.$id_kontrak.' AND '.$av_bw.' BETWEEN berat_a AND berat_b',FALSE);
        $harga='Belum ditentukan';
        if($get->num_rows()>0){
            $row=$get->row();
            $harga=$row->harga;
        }
        $data['harga_kontrak']=$harga;
        $response['data']=$data;
        echo json_encode($response);
    }
    function getStok(){
        $id_jual=$this->input->get('id_jual');
        $id_kontrak=$this->input->get('id_kontrak');
        $sisaDataAyam=sisaDataAyam($id_kontrak,[],'1');
        if($id_jual!=''){
            $this->db->where('id_jual !=',$id_jual);
        }
        $sisaJual=jual($id_kontrak,[],'1');
        echo $sisaDataAyam-$sisaJual;
    }
}