<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pangkatgol extends CI_Controller {

##################################
##          CONSTRUCT           ##
##################################

public function __construct()
{
	parent::__construct();
	$this->load->model('pangkatgolModel');
}
##################################
##          VIEW DEFAULT        ##
##################################
public function index(){
	$get_data=$this->pangkatgolModel->get_data();
	$template = array(
		'table_open' => '<table border="0" cellpadding="4" cellspacing="0" class="table table-bordered dt-responsive display" id="table-dt">',
	);
	$this->table->set_template($template);
	$this->table->set_heading('No','gol','pangkat','urutan','','');
	$i=1;
	foreach($get_data->result() as $row){
		$this->table->add_row(array("data"=>$i,"width"=>"50px","align"=>"center"),
													$row->gol,
													$row->pangkat,
													$row->urutan,
													array("data"=>anchor(site_url("pangkatgol/sunting?id_pangkat_gol=".$row->id_pangkat_gol),"<i class='fa fa-edit'></i> Ubah",["class"=>"btn btn-xs btn-info btn-sunting","onclick"=>false]),"width"=>"20px","align"=>"center"),
													array("data"=>anchor(site_url("pangkatgol/hapus?id_pangkat_gol=".$row->id_pangkat_gol),"<i class='fa fa-trash'></i> Hapus",["class"=>"btn btn-xs btn-danger","onclick"=>"return confirm('Yakin Hapus Data?')"]),"width"=>"20px","align"=>"center"));
		$i++;
	}
	$databody['table']=$this->table->generate();
	$data['title']='Data Pangkat Golongan';
	$data['body']=$this->load->view('pangkatgolView',$databody,true);
	$data['js']=$this->load->view('js/pangkatgolJs',$databody,true);
	$this->load->view('templated/html',$data);
}
##################################
##           SUNTING            ##
##################################

public function sunting(){
	$where=[
			'id_pangkat_gol'=>$_GET['id_pangkat_gol']
			];
	$row=$this->db->get_where('pangkat_gol',$where)->row_array();
	$response['data']=$row;
	header('Content-type: application/json;charset=utf-8');
	echo json_encode($response,JSON_PRETTY_PRINT);
}

##################################
##            SIMPAN            ##
##################################

public function simpan(){
	if($this->input->post('simpan')){
		$data=$this->_datapost();
		$this->pangkatgolModel->insert($data);
	}
	redirect('pangkatgol');
}
##################################
##            UBAH              ##
##################################

public function ubah(){
	if($this->input->post('simpan')){
		$data=$this->_datapost();
		$id_pangkat_gol=$this->input->post('id_pangkat_gol');
		$where=array(
			'id_pangkat_gol'=>$id_pangkat_gol
		);
		$this->pangkatgolModel->update($data,$where);
	}
	redirect('Pangkatgol');
}

##################################
##            HAPUS             ##
##################################

public function hapus(){
	$where=array(
			'id_pangkat_gol'=>$this->input->get(id_pangkat_gol)
		);
	$this->pangkatgolModel->delete($where);
	redirect('pangkatgol');
}
##################################
##      KUMPULKAN NILAI POST    ##
##################################

private function _datapost(){
	$id_pangkat_gol = $this->input->post('id_pangkat_gol');
	$gol = $this->input->post('gol');
	$pangkat = $this->input->post('pangkat');
	$urutan = $this->input->post('urutan');
	$data=array(
			'id_pangkat_gol'=>$id_pangkat_gol,
			'gol'=>$gol,
			'pangkat'=>$pangkat,
			'urutan'=>$urutan
		);
	return $data;
}
//end class
}
