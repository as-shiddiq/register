<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Rb1 extends CI_Controller {

##################################
##          CONSTRUCT           ##
##################################

public function __construct()
{
	parent::__construct();
	$this->load->model('rb1Model');
}
##################################
##          VIEW DEFAULT        ##
##################################
public function index(){
	$bulan=isset($_GET['bulan'])?$_GET['bulan']:date('m');
	$tahun=isset($_GET['tahun'])?$_GET['tahun']:date('Y');

	if($bulan!=''){
        $this->db->where("DATE_FORMAT(a.tgl,'%m')='".$bulan."'");
    }
    if($tahun!=''){
        $this->db->where("DATE_FORMAT(a.tgl,'%Y')='".$tahun."'");
    }
	$get_data=$this->rb1Model->get_data();
	$template = array(
		'table_open' => '<table border="0" cellpadding="4" cellspacing="0" class="table table-bordered dt-responsive display" id="table-dt">',
	);
	$this->table->set_template($template);
	$this->table->set_heading('No','tgl','nm tersangka','no RP3','jumlah & jenis sitaan','tmpt penyimpanan','SP Penyitaan No. Tgl & petugas penyitaan','disita dari','no & tgl persetujuan','no & tgl sp lelang','no & tgl penetapan','jmlh hsl lelang','sitaan disisihkan','keterangan','','');
	$i=1;
	foreach($get_data->result() as $row){
		$this->table->add_row(array("data"=>$i,"width"=>"50px","align"=>"center"),
													$row->tgl,
													$row->nama_lengkap,
													$row->no_rp3,
													$row->jumlah_sitaan.'<br>'.$row->jenis_sitaan,
													$row->tmpt_penyimpanan,
													$row->no_penyitaan.'<br>'.$row->tgl_penyitaan.'<br>'.$row->petugas_penyitaan,
													$row->disita_dari,
													$row->no_persetujuan.'<br>'.$row->tgl_persetujuan,
													$row->no_sp_lelang.'<br>'.$row->tgl_sp_lelang,
													$row->no_penetapan.'<br>'.$row->tgl_penetapan,
													$row->jmlh_hsl_lelang,
													$row->sitaan_disisihkan,
													$row->keterangan,
													array("data"=>anchor(site_url("rb1?ubah&no_rb1=".$row->no_rb1),"<i class='fa fa-edit'></i> Ubah",["class"=>"btn btn-xs btn-info btn-sunting","onclick"=>false]),"width"=>"20px","align"=>"center"),
													array("data"=>anchor(site_url("rb1/hapus?no_rb1=".$row->no_rb1),"<i class='fa fa-trash'></i> Hapus",["class"=>"btn btn-xs btn-danger","onclick"=>"return confirm('Yakin Hapus Data?')"]),"width"=>"20px","align"=>"center"));
		$i++;
	}
	$databody['table']=$this->table->generate();
	$data['title']=rbConvert(1,0).'-'.rbConvert(1,1);
	$data['body']=$this->load->view('rb1View',$databody,true);
	$data['js']=$this->load->view('js/rb1Js',$databody,true);
	$this->load->view('templated/html',$data);
}

##################################
##            SIMPAN            ##
##################################

public function simpan(){
	if($this->input->post('simpan')){
		$data=$this->_datapost();
		$this->rb1Model->insert($data);
	}
	redirect('rb1');
}
##################################
##            UBAH              ##
##################################

public function ubah(){
	if($this->input->post('simpan')){
		$data=$this->_datapost();
		$no_rb1=$this->input->post('no_rb1');
		$where=array(
			'no_rb1'=>$no_rb1
		);
		$this->rb1Model->update($data,$where);
	}
	redirect('Rb1');
}

##################################
##            HAPUS             ##
##################################

public function hapus(){
	$where=array(
			'no_rb1'=>$this->input->get(no_rb1)
		);
	$this->rb1Model->delete($where);
	redirect('rb1');
}
##################################
##      KUMPULKAN NILAI POST    ##
##################################

private function _datapost(){
	$no_rb1 = $this->input->post('no_rb1');
	$tgl = $this->input->post('tgl');
	$no_rp3 = $this->input->post('no_rp3');
	$jumlah_sitaan = $this->input->post('jumlah_sitaan');
	$jenis_sitaan = $this->input->post('jenis_sitaan');
	$tmpt_penyimpanan = $this->input->post('tmpt_penyimpanan');
	$no_penyitaan = $this->input->post('no_penyitaan');
	$tgl_penyitaan = $this->input->post('tgl_penyitaan');
	$petugas_penyitaan = $this->input->post('petugas_penyitaan');
	$disita_dari = $this->input->post('disita_dari');
	$no_persetujuan = $this->input->post('no_persetujuan');
	$tgl_persetujuan = $this->input->post('tgl_persetujuan');
	$no_sp_lelang = $this->input->post('no_sp_lelang');
	$tgl_sp_lelang = $this->input->post('tgl_sp_lelang');
	$no_penetapan = $this->input->post('no_penetapan');
	$tgl_penetapan = $this->input->post('tgl_penetapan');
	$jmlh_hsl_lelang = $this->input->post('jmlh_hsl_lelang');
	$sitaan_disisihkan = $this->input->post('sitaan_disisihkan');
	$keterangan = $this->input->post('keterangan');
	$tgl_simpan = $this->input->post('tgl_simpan');
	$data=array(
			'no_rb1'=>$no_rb1,
			'tgl'=>$tgl,
			'no_rp3'=>$no_rp3,
			'jumlah_sitaan'=>$jumlah_sitaan,
			'jenis_sitaan'=>$jenis_sitaan,
			'tmpt_penyimpanan'=>$tmpt_penyimpanan,
			'no_penyitaan'=>$no_penyitaan,
			'tgl_penyitaan'=>$tgl_penyitaan,
			'petugas_penyitaan'=>$petugas_penyitaan,
			'disita_dari'=>$disita_dari,
			'no_persetujuan'=>$no_persetujuan,
			'tgl_persetujuan'=>$tgl_persetujuan,
			'no_sp_lelang'=>$no_sp_lelang,
			'tgl_sp_lelang'=>$tgl_sp_lelang,
			'no_penetapan'=>$no_penetapan,
			'tgl_penetapan'=>$tgl_penetapan,
			'jmlh_hsl_lelang'=>$jmlh_hsl_lelang,
			'sitaan_disisihkan'=>$sitaan_disisihkan,
			'keterangan'=>$keterangan,
			'tgl_simpan'=>$tgl_simpan
		);
	return $data;
}
//end class
}
