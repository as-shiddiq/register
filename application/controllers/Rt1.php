<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Rt1 extends CI_Controller {

##################################
##          CONSTRUCT           ##
##################################

public function __construct()
{
	parent::__construct();
	$this->load->model('rt1Model');
}
##################################
##          VIEW DEFAULT        ##
##################################
public function index(){
	$get_data=$this->rt1Model->get_data();
	$template = array(
		'table_open' => '<table border="0" cellpadding="4" cellspacing="0" class="table table-bordered dt-responsive display" id="table-dt">',
	);
	$this->table->set_template($template);
	$this->table->set_heading('No','nama lengkap','tgl reg perkara','pasal disangkakan','jns penahanan','tgl ditahan','tgl 24kuhap sejak','tgl 29kuhap sejak','tgl pengalihan','dikeluarkan demi hukum','no & tgl penangguhan','jaminan penangguhan','tgl pencabutan penangguhan','penyelesaian','keterangan','tgl simpan','','');
	$i=1;
	foreach($get_data->result() as $row){
		$this->table->add_row(array("data"=>$i,"width"=>"50px","align"=>"center"),
													$row->nama_lengkap,
													$row->tgl_reg_perkara,
													$row->pasal_disangkakan,
													$row->jns_penahanan,
													$row->tgl_ditahan,
													$row->tgl_24kuhap_sejak,
													$row->tgl_29kuhap_sejak,
													$row->tgl_pengalihan,
													$row->dikeluarkan_demi_hukum,
													$row->no_pengguhan.'<br>'.$row->tgl_penangguhan,
													$row->jaminan_penangguhan,
													$row->tgl_pencabutan_penangguhan,
													$row->penyelesaian,
													$row->keterangan,
													$row->tgl_simpan,
													array("data"=>anchor(site_url("rt1?ubah&no_rt1=".$row->no_rt1),"<i class='fa fa-edit'></i> Ubah",["class"=>"btn btn-xs btn-info btn-sunting","onclick"=>false]),"width"=>"20px","align"=>"center"),
													array("data"=>anchor(site_url("rt1/hapus?no_rt1=".$row->no_rt1),"<i class='fa fa-trash'></i> Hapus",["class"=>"btn btn-xs btn-danger","onclick"=>"return confirm('Yakin Hapus Data?')"]),"width"=>"20px","align"=>"center"));
		$i++;
	}
	$databody['table']=$this->table->generate();
	$data['title']=rtConvert(1,0).'-'.rtConvert(1,1);
	$data['body']=$this->load->view('rt1View',$databody,true);
	$data['js']=$this->load->view('js/rt1Js',$databody,true);
	$this->load->view('templated/html',$data);
}

##################################
##            SIMPAN            ##
##################################

public function simpan(){
	if($this->input->post('simpan')){
		$data=$this->_datapost();
		$this->rt1Model->insert($data);
	}
	redirect('rt1');
}
##################################
##            UBAH              ##
##################################

public function ubah(){
	if($this->input->post('simpan')){
		$data=$this->_datapost();
		$no_rt1=$this->input->post('no_rt1');
		$where=array(
			'no_rt1'=>$no_rt1
		);
		$this->rt1Model->update($data,$where);
	}
	redirect('Rt1');
}

##################################
##            HAPUS             ##
##################################

public function hapus(){
	$where=array(
			'no_rt1'=>$this->input->get(no_rt1)
		);
	$this->rt1Model->delete($where);
	redirect('rt1');
}
##################################
##      KUMPULKAN NILAI POST    ##
##################################

private function _datapost(){
	$no_rt1 = $this->input->post('no_rt1');
	$nik = $this->input->post('nik');
	$no_rp3 = $this->input->post('no_rp3');
	$pasal_disangkakan = $this->input->post('pasal_disangkakan');
	$jns_penahanan = $this->input->post('jns_penahanan');
	$tgl_ditahan = $this->input->post('tgl_ditahan');
	$tgl_24kuhap_sejak = $this->input->post('tgl_24kuhap_sejak');
	$tgl_29kuhap_sejak = $this->input->post('tgl_29kuhap_sejak');
	$tgl_pengalihan = $this->input->post('tgl_pengalihan');
	$dikeluarkan_demi_hukum = $this->input->post('dikeluarkan_demi_hukum');
	$no_pengguhan = $this->input->post('no_pengguhan');
	$tgl_penangguhan = $this->input->post('tgl_penangguhan');
	$jaminan_penangguhan = $this->input->post('jaminan_penangguhan');
	$tgl_pencabutan_penangguhan = $this->input->post('tgl_pencabutan_penangguhan');
	$penyelesaian = $this->input->post('penyelesaian');
	$keterangan = $this->input->post('keterangan');
	$tgl_simpan = $this->input->post('tgl_simpan');
	$data=array(
			'no_rt1'=>$no_rt1,
			'nik'=>$nik,
			'no_rp3'=>$no_rp3,
			'pasal_disangkakan'=>$pasal_disangkakan,
			'jns_penahanan'=>$jns_penahanan,
			'tgl_ditahan'=>$tgl_ditahan,
			'tgl_24kuhap_sejak'=>$tgl_24kuhap_sejak,
			'tgl_29kuhap_sejak'=>$tgl_29kuhap_sejak,
			'tgl_pengalihan'=>$tgl_pengalihan,
			'dikeluarkan_demi_hukum'=>$dikeluarkan_demi_hukum,
			'no_pengguhan'=>$no_pengguhan,
			'tgl_penangguhan'=>$tgl_penangguhan,
			'jaminan_penangguhan'=>$jaminan_penangguhan,
			'tgl_pencabutan_penangguhan'=>$tgl_pencabutan_penangguhan,
			'penyelesaian'=>$penyelesaian,
			'keterangan'=>$keterangan,
			'tgl_simpan'=>$tgl_simpan
		);
	return $data;
}
//end class
}
