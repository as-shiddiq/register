<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Log extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct()
	   {
	        parent::__construct();
			// if($this->session->userdata('logged')!=true){
			// 	redirect('login');
			// }
			$this->load->model('userModel');
	   }
	public function index($a='')
	{
		$data['title']='Log In';
		$this->load->view('LogView',$data);
	}
	
	public function check($a="",$b="")
	{
		$username=$this->input->post('username');
		$kata_sandi=$this->input->post('password');
		//cek user
		$this->db->where(['username'=>$username,'password'=>$kata_sandi]);
		$cek=$this->userModel->get_data();
		if($cek->num_rows()==1){
			$row=$cek->row_array();
			$this->session->set_userdata('logged','ya');
			$this->session->set_userdata($row);
			$this->session->set_flashdata('info',info_success(icon('smile-o').' Selamat Datang <b>'.$row['surel'].'</b>'));
			redirect('home');
		}
		else{
			$this->session->set_flashdata('info',info_danger(icon('times').' Data Akun tidak dapat ditemukan di dalam basisdata'));
			redirect('log');
		}

	}
	function out(){
		$this->session->sess_destroy();
		if(isset($_GET['to'])){
			redirect($_GET['to']);
		}
		else{
			redirect('log');
		}
	}

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
