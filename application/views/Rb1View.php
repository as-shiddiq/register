<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
  <?php if(isset($_GET['tambah']) OR isset($_GET['ubah'])){ ?>    <!--OPEN FORM -->
  <div class="card">
	<div class="card-header">
		<h3 class="card-title">FORM RB1</h3>

		<div class="card-tools">
		<button type="button" class="btn btn-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
			<i class="fa fa-minus"></i></button>
		</div>
	</div>
	<div class="card-body">

	<!-- form -->
<?php
if(isset($_GET['ubah'])){
  $parameter='ubah';
  $where=array(
          	'no_rb1'=>$_GET['no_rb1']
	        );
  $row=$this->db->get_where('rb1',$where)->row_array();
  extract($row);
}
else{
  $parameter='simpan';
}
?>
<form class="validate form-horizontal" id="form-rb1" method="POST" action="<?=site_url()?>rb1/<?=$parameter;?>">
<div class="col-md-12">
	<div class="row">
	<div class="col-lg-6">
		<div class="form-group">
			<label>No Urut Register</label>
			<?=input_text('no_rb1',($no_rb1??''),'md-input','required');?>
		</div>
	</div>
							
	<div class="col-lg-6">
		<div class="form-group">
			<label>Tanggal</label>
			<?=input_date('tgl_penetapan',($tgl_penetapan??''),'md-input','required');?>
		</div>
	</div>
	</div>
</div>					
<div class="col-lg-12">
	<div class="form-group">
		<label>Nomor RP3</label>
		<?php 
			$op=NULL;
			$op['']='Pilih Salah Satu';  
			$this->db->order_by('tgl_reg_perkara','ASC');
			$data=$this->db->get('rp3');
			foreach($data->result() as $row){
				$op[$row->no_rp3]=$row->tgl_reg_perkara;
			}
			echo select('no_rp3',$op,($no_rp3??''),'select2','required');?>
	</div>
</div>
<div class="col-md-12">
	<div class="row">
		<div class="col-lg-6">
			<div class="form-group">
				<label>Jumlah Sitaan</label>
				<?=input_text('jumlah_sitaan',($jumlah_sitaan??''),'md-input','required');?>
			</div>
		</div>
								
		<div class="col-lg-6">
			<div class="form-group">
				<label>Jenis Sitaan</label>
				<?=input_text('jenis_sitaan',($jenis_sitaan??''),'md-input','required');?>
			</div>
		</div>
			
	</div>
</div>					
<div class="col-lg-12">
	<div class="form-group">
		<label>Tempat Penyimpanan</label>
		<?=input_text('tmpt_penyimpanan',($tmpt_penyimpanan??''),'md-input','required');?>
	</div>
</div>
<div class="col-md-12">
	<div class="row">
		<div class="col-lg-4">
			<div class="form-group">
				<label>No SP Penyitaan </label>
				<?=input_text('no_penyitaan',($no_penyitaan??''),'md-input','required');?>
			</div>
		</div>
							
		<div class="col-lg-4">
			<div class="form-group">
				<label>Tanggal </label>
				<?=input_date('tgl_penyitaan',($tgl_penyitaan??''),'md-input','required');?>
			</div>
		</div>
								
		<div class="col-lg-4">
			<div class="form-group">
				<label>Petugas Yang Melakukan Penyitaan</label>
				<?=input_text('petugas_penyitaan',($petugas_penyitaan??''),'md-input','required');?>
			</div>
		</div>
	</div>
</div>			
<div class="col-lg-12">
	<div class="form-group">
		<label>Disita Dari</label>
		<?=input_text('disita_dari',($disita_dari??''),'md-input','required');?>
	</div>
</div>

<div class="col-md-12">
	<div class="row">
		<div class="col-lg-6">
			<div class="form-group">
				<label>Ijin Persetujuan Ketua PN Nomor</label>
				<?=input_text('no_persetujuan',($no_persetujuan??''),'md-input','required');?>
			</div>	
		</div>	
		<div class="col-lg-6">
			<div class="form-group">
				<label>Tanggal</label>
				<?=input_date('tgl_persetujuan',($tgl_persetujuan??''),'md-input','required');?>
			</div>
		</div>
		</div>
</div>			
<div class="col-md-12">
	<div class="row">
		<div class="col-lg-6">
			<div class="form-group">
				<label>SP Lelang  (Ps. 45 KUHAP) Nomor</label>
				<?=input_text('no_sp_lelang',($no_sp_lelang??''),'md-input','required');?>
			</div>
		</div>	
		<div class="col-lg-6">
			<div class="form-group">
				<label>Tanggal</label>
				<?=input_date('tgl_sp_lelang',($tgl_sp_lelang??''),'md-input','required');?>
			</div>
		</div>
		</div>
</div>				
<div class="col-md-12">
	<div class="row">
	<div class="col-lg-6">
		<div class="form-group">
			<label>Nomor Penetapan Pengadilan</label>
			<?=input_text('no_penetapan',($no_penetapan??''),'md-input','required');?>
		</div>
	</div>
						
	<div class="col-lg-6">
		<div class="form-group">
			<label>Tanggal</label>
			<?=input_date('tgl_penetapan',($tgl_penetapan??''),'md-input','required');?>
		</div>
	</div>
</div>
</div>
			
<div class="col-lg-12">
	<div class="form-group">
		<label>Jumlah Hasil Lelang</label>
		<?=input_text('jmlh_hsl_lelang',($jmlh_hsl_lelang??''),'md-input','required');?>
	</div>
</div>
						
<div class="col-lg-12">
	<div class="form-group">
		<label>Benda Sitaan Yang Disisihkan</label>
		<?=input_text('sitaan_disisihkan',($sitaan_disisihkan??''),'md-input','required');?>
	</div>
</div>
						
<div class="col-lg-12">
	<div class="form-group">
		<label>Keterangan</label>
		<?=textarea('keterangan',($keterangan??''),'md-input','required');?>
	</div>
</div>
<?=input_hidden('tgl_simpan',($tgl_simpan??''),'','required');?><!--endform-->
  <div class="col-lg-12">
    <div class="form-group">
      <label></label>
      <button type="submit" name="simpan" class="btn btn-primary" value="true">Simpan</button>
      <a href="javascript:history.go(-1)" class="btn btn-danger">Batal</a>
    </div>
  </div>
  <div class="clearfix"></div>
  </div>
</form>
  	</div>
</div>

  <?php } else { ?>
<!--OPEN TABLE-->
<div class="card">
	<div class="card-header">
		<h3 class="card-title">DATA RB1</h3>

		<div class="card-tools">
		<button type="button" class="btn btn-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
			<i class="fa fa-minus"></i></button>
		</div>
	</div>
	<div class="card-body">
		<!-- table -->
		<?php echo anchor(site_url('rb1?tambah'),"<i class='fa fa-plus'></i> Tambah",['class'=>'btn btn-success btn-tambah']);?>
		
		<hr>
		 <form>
            <?php
                $bulan=isset($_GET['bulan'])?$_GET['bulan']:date('m');
                $tahun=isset($_GET['tahun'])?$_GET['tahun']:date('Y');
            ?>
            <div class="row">
                <div class="col-md-4">
                    <?=select('bulan',bulanList(),$bulan)?>
                </div>
                <div class="col-md-4">
                    <?=select('tahun',tahunList(),$tahun)?>
                </div>
                <div class="col-md-4">
                    <button class="btn btn-info">Lihat</button>
                    <a href="<?=site_url('cetak')?>?jenis=rb1&bulan=<?=$bulan?>&tahun=<?=$tahun?>" class="btn btn-warning" target="_blank">Cetak</a>
                </div>
            </div>
        </form>
        <hr>
		<?php echo $this->session->flashdata('info');?>
		<?php echo $table;?>
		<!-- end table -->
	</div>
</div>

  <?php }

