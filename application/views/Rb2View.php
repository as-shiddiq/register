<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
  <?php if(isset($_GET['tambah']) OR isset($_GET['ubah'])){ ?>    <!--OPEN FORM -->
  <div class="card">
	<div class="card-header">
		<h3 class="card-title">FORM RB2</h3>

		<div class="card-tools">
		<button type="button" class="btn btn-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
			<i class="fa fa-minus"></i></button>
		</div>
	</div>
	<div class="card-body">

	<!-- form -->
<?php
if(isset($_GET['ubah'])){
  $parameter='ubah';
  $where=array(
          	'no_rb2'=>$_GET['no_rb2']
	        );
  $row=$this->db->get_where('rb2',$where)->row_array();
  extract($row);
}
else{
  $parameter='simpan';
}
?>
<form class="validate form-horizontal" id="form-rb2" method="POST" action="<?=site_url()?>rb2/<?=$parameter;?>">
<div class="col-md-12">
	<div class="row">
	<div class="col-lg-6">
		<div class="form-group">
			<label>No Urut Register</label>
			<?=input_text('no_rb2',($no_rb2??''),'md-input','required');?>
		</div>
	</div>
							
	<div class="col-lg-6">
		<div class="form-group">
			<label>Registrasi Perkara</label>
			<?=input_text('reg_perkara',($reg_perkara??''),'md-input','required');?>
		</div>
	</div>
	</div>
</div>					
<div class="col-lg-12">
	<div class="form-group">
		<label>Tgl. Penerimaan Tanggung Jawab Barang Bukti Penyidik</label>
		<?=input_date('tgl_penerimaan_tgg_jwb',($tgl_penerimaan_tgg_jwb??''),'md-input','required');?>
	</div>
</div>
						
<div class="col-lg-12">
	<div class="form-group">
		<label>Jaksa Penuntut Umum</label>
		<?=input_text('jpu',($jpu??''),'md-input','required');?>
	</div>
</div>
						
<div class="col-lg-12">
	<div class="form-group">
		<label>Nama Tersangka</label>
		<?php 
			$op=NULL;
			$op['']='Pilih Salah Satu';  
			$this->db->order_by('reg_perkara','ASC');
			$data=$this->db->get('rb2');
			foreach($data->result() as $row){
				$op[$row->no_rb2]=$row->reg_perkara;
			}
			echo select('nik',$op,($nik??''),'','required');?>
	</div>
</div>
						
<div class="col-lg-12">
	<div class="form-group">
		<label>Pasal Yang Didakwakan</label>
		<?=input_text('psl_didakwakan',($psl_didakwakan??''),'md-input','required');?>
	</div>
</div>
						
<div class="col-lg-12">
	<div class="form-group">
		<label>Jumlah, Ukuran, Jenis</label>
		<?=textarea('jml_ukrn_jns',($jml_ukrn_jns??''),'md-input','required');?>
	</div>
</div>
						
<div class="col-lg-12">
	<div class="form-group">
		<label>Penyimpanan/Penitipan/Pelelangan, Pemusnahan Barang Bukti (No./Tgl. Print, Tempat, Tgl.BA</label>
		<?=textarea('penyimpanan',($penyimpanan??''),'md-input','required');?>
	</div>
</div>
						
<div class="col-lg-12">
	<div class="form-group">
		<label>Tgl. Penyerahan Kepada PN (Pelimpahan APB/APS)</label>
		<?=input_text('tgl_penyerahan',($tgl_penyerahan??''),'md-input','required');?>
	</div>
</div>

<div class="col-md-12">
	<div class="row">
		<div class="col-lg-6">
			<div class="form-group">
				<label>No. Amar Putusan PN</label>
				<?=input_text('no_putusan_pn',($no_putusan_pn??''),'md-input','required');?>
			</div>
		</div>
								
		<div class="col-lg-6">
			<div class="form-group">
				<label>Tanggal</label>
				<?=input_date('tgl_putusan_pn',($tgl_putusan_pn??''),'md-input','required');?>
			</div>
		</div>
		</div>
</div>				
<div class="col-md-12">
	<div class="row">
		<div class="col-lg-6">
			<div class="form-group">
				<label>No. Amar Putusan PT</label>
				<?=input_text('no_putusan_pt',($no_putusan_pt??''),'md-input','required');?>
			</div>
		</div>
								
		<div class="col-lg-6">
			<div class="form-group">
				<label>Tanggal</label>
				<?=input_date('tgl_putusan_pt',($tgl_putusan_pt??''),'md-input','required');?>
			</div>
		</div>
		</div>
</div>		

<div class="col-md-12">
	<div class="row">
		<div class="col-lg-6">
		<div class="form-group">
			<label>No. Amar Putusan MA</label>
			<?=input_text('no_putusan_ma',($no_putusan_ma??''),'md-input','required');?>
		</div>
	</div>
							
	<div class="col-lg-6">
		<div class="form-group">
			<label>Tanggal</label>
			<?=input_date('tgl_putusan_ma',($tgl_putusan_ma??''),'md-input','required');?>
		</div>
	</div>
	</div>
</div>
					
<div class="col-lg-12">
	<div class="form-group">
		<label>BA Pelaksanaan Putusan PN/PT/MA (Tgl. Dikembalikan, Dirampas, Dimusnahkan, Dll)</label>
		<?=textarea('tgl_ba_plksanaan_ptusan',($tgl_ba_plksanaan_ptusan??''),'md-input','required');?>
	</div>
</div>
<div class="col-md-12">
<h4>   BARANG BUKTI YANG TIDAK DIAMBIL </h4>
</div>		
<div class="col-md-12">
	<div class="row">
		<div class="col-lg-6">
			<div class="form-group">
				<label>No Pengumuman</label>
				<?=input_text('no_penguguman',($no_penguguman??''),'md-input','required');?>
			</div>
		</div>
								
		<div class="col-lg-6">
			<div class="form-group">
				<label>Tanggal</label>
				<?=input_date('tgl_pengumuman',($tgl_pengumuman??''),'md-input','required');?>
			</div>
		</div>
		</div>
</div>				
<div class="col-md-12">
	<div class="row">
		<div class="col-lg-6">
			<div class="form-group">
				<label>No Permohonan Ijin Lelang</label>
				<?=input_text('no_ijin_lelang',($no_ijin_lelang??''),'md-input','required');?>
			</div>
		</div>
								
		<div class="col-lg-6">
			<div class="form-group">
				<label>Tanggal </label>
				<?=input_date('tgl_ijin_lelang',($tgl_ijin_lelang??''),'md-input','required');?>
			</div>
		</div>
	</div>
</div>					
<div class="col-lg-12">
	<div class="form-group">
		<label>Tanggal BA Penyerahan Ke Pembinaan</label>
		<?=input_date('tgl_ba_penyerahan',($tgl_ba_penyerahan??''),'md-input','required');?>
	</div>
</div>
<div class="col-md-12">
	<div class="row">
		<div class="col-lg-6">
			<div class="form-group">
				<label>Pemanfaatan ( No. Permohonan Ijin, Kepada)</label>
				<?=input_text('no_pemanfaatan',($no_pemanfaatan??''),'md-input','required');?>
			</div>
		</div>
								
		<div class="col-lg-6">
			<div class="form-group">
				<label>Tanggal</label>
				<?=input_date('tgl_pemanfaatan',($tgl_pemanfaatan??''),'md-input','required');?>
			</div>
		</div>
		</div>
</div>				
<div class="col-lg-12">
	<div class="form-group">
		<label>Penyerahan (Tgl. BA, Asal Instansi)</label>
		<?=input_date('tgl_ba_instansi',($tgl_ba_instansi??''),'md-input','required');?>
	</div>
</div>
<h4>   BARANG TEMUAN </h4>
<div class="col-md-12">
	<div class="row">
		<div class="col-lg-6">
			<div class="form-group">
				<label>No. Pengumuman </label>
				<?=input_text('no_pengumuman_bt',($no_pengumuman_bt??''),'md-input','required');?>
			</div>
		</div>
								
		<div class="col-lg-6">
			<div class="form-group">
				<label>Tanggal</label>
				<?=input_date('tgl_pengumuman_bt',($tgl_pengumuman_bt??''),'md-input','required');?>
			</div>
		</div>
	</div>
</div>			
<div class="col-md-12">
	<div class="row">
	<div class="col-lg-6">
		<div class="form-group">
			<label>No. Permohonan Izin Lelang</label>
			<?=input_text('no_izin_lelang',($no_izin_lelang??''),'md-input','required');?>
		</div>
	</div>
							
	<div class="col-lg-6">
		<div class="form-group">
			<label>Tanggal</label>
			<?=input_date('tgl_izin_lelang',($tgl_izin_lelang??''),'md-input','required');?>
		</div>
	</div>
</div>
</div>				
<div class="col-lg-12">
	<div class="form-group">
		<label>Tgl. BA Penyerahan Ke Pembinaan BT</label>
		<?=input_date('tgl_ba_penyerahan_bt',($tgl_ba_penyerahan_bt??''),'md-input','required');?>
	</div>
</div>
<div class="col-md-12">
	<div class="row">
		<div class="col-lg-6">
			<div class="form-group">
				<label>Pemanfaatan (No. Permohonan Ijin, Kepada</label>
				<?=input_text('no_pemanfaatan_bt',($no_pemanfaatan_bt??''),'md-input','required');?>
			</div>
		</div>
								
		<div class="col-lg-6">
			<div class="form-group">
				<label>Tanggal</label>
				<?=input_date('tgl_pemanfaatan_bt',($tgl_pemanfaatan_bt??''),'md-input','required');?>
			</div>
		</div>
		</div>
</div>				
<div class="col-lg-12">
	<div class="form-group">
		<label>Keterangan</label>
		<?=textarea('keterangan',($keterangan??''),'md-input','required');?>
	</div>
</div>
<?=input_hidden('tgl_simpan',($tgl_simpan??''),'','required');?><!--endform-->
  <div class="col-lg-12">
    <div class="form-group">
      <label></label>
      <button type="submit" name="simpan" class="btn btn-primary" value="true">Simpan</button>
      <a href="javascript:history.go(-1)" class="btn btn-danger">Batal</a>
    </div>
  </div>
  <div class="clearfix"></div>
  </div>
</form>
  	</div>
</div>

  <?php } else { ?>
<!--OPEN TABLE-->
<div class="card">
	<div class="card-header">
		<h3 class="card-title">DATA RB2</h3>

		<div class="card-tools">
		<button type="button" class="btn btn-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
			<i class="fa fa-minus"></i></button>
		</div>
	</div>
	<div class="card-body">

					<!-- table -->
		<?php echo anchor(site_url('rb2?tambah'),"<i class='fa fa-plus'></i> Tambah",['class'=>'btn btn-success btn-tambah']);?>
					
					<hr>
					<?php echo $this->session->flashdata('info');?>
					<div style="width:100%;overflow:scroll">
						<?php echo $table;?>
					</div>

					<!-- end table -->
	</div>
</div>

  <?php }

