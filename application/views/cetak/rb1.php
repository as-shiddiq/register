<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
	<?php
	include 'load/style.php';
	?>
	<h4>KEJAKSAAN NEGERI PELAIHARI</h4>

	<h4 class="center">REGISTER BENDA SITAAN
		<br>
		TINDAK PIDANA

	</h4>
	<table id="table">
		<thead>
			<tr>
				<th rowspan="2">No Urut</th>
				<th rowspan="2">No. & Tgl. Register Benda Sitaan</th>
				<th rowspan="2">Nama tersangka</th>
				<th rowspan="2">No. Register Perkara Tahap Penyidikan </th>
				<th rowspan="2">Jumlah dan Jenis Benda Sitaan</th>
				<th rowspan="2">Tempat Penyimpanan</th>
				<th colspan="3">Penyitaan</th>
				<th rowspan="2">SP lelang (Ps. 45 KUHAP) No. & Tgl.</th>
				<th rowspan="2">Penetapan pengadilan No. & Tgl isi Penetapan.</th>
				<th rowspan="2">Jumlah Hasil Lelang Ps.45 KUHAP</th>
				<th rowspan="2">Benda Sitaan yang disisihkan</th>
				<th rowspan="2">Keterangan</th>
			</tr>
			<tr>
				<th class="no-border-left">SP Peniyataan No, Tgl & Petugads yang melaksanakan penyitaan</th>
				<th>Disita Dari</th>
				<th class="border-right">Ijin/persetujuan Ketua PN No & Tgl.</th>
			</tr>
		</thead>
		<tbody>
			

	<?php

	$bulan=isset($_GET['bulan'])?$_GET['bulan']:date('m');
	$tahun=isset($_GET['tahun'])?$_GET['tahun']:date('Y');
	if($bulan!=''){
        $this->db->where("DATE_FORMAT(a.tgl,'%m')='".$bulan."'");
    }
    if($tahun!=''){
        $this->db->where("DATE_FORMAT(a.tgl,'%Y')='".$tahun."'");
    }
	$get_data=$this->Rb1Model->get_data();
	$i=1;
	foreach($get_data->result() as $row){
		?>
		<tr>
			<td><?=$row->no_rb1?></td>
			<td><?=$row->tgl?></td>
			<td><?=$row->nama_lengkap?></td>
			<td><?=$row->no_rp3?></td>
			<td><?=$row->jumlah_sitaan.'<br>'.$row->jenis_sitaan?></td>
			<td><?=$row->tmpt_penyimpanan?></td>
			<td><?=$row->no_penyitaan.'<br>'.$row->tgl_penyitaan.'<br>'.$row->petugas_penyitaan?></td>
			<td><?=$row->disita_dari?></td>
			<td><?=$row->no_persetujuan.'<br>'.$row->tgl_persetujuan?></td>
			<td><?=$row->no_sp_lelang.'<br>'.$row->tgl_sp_lelang?></td>
			<td><?=$row->no_penetapan.'<br>'.$row->tgl_penetapan?></td>
			<td><?=$row->jmlh_hsl_lelang?></td>
			<td><?=$row->sitaan_disisihkan?></td>
			<td><?=$row->keterangan?></td>

		</tr>
		$i++;
		<?php
	}
	?>

		</tbody>
	</table>
	<small style="color:#999;font-size: 10px">
	generated at :<?=date('Y-m-d H:i:s')?>
	</small>
</body>
</html>