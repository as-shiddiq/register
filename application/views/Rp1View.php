<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
  <?php if(isset($_GET['tambah']) OR isset($_GET['ubah'])){ ?>    <!--OPEN FORM -->
  <div class="card">
	<div class="card-header">
		<h3 class="card-title">FORM - Register <?=rpConvert('1',1)?></h3>

		<div class="card-tools">
		<button type="button" class="btn btn-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
			<i class="fa fa-minus"></i></button>
		</div>
	</div>
	<div class="card-body">

	<!-- form -->
<?php
if(isset($_GET['ubah'])){
  $parameter='ubah';
  $where=array(
          	'no_rp1'=>$_GET['no_rp1']
	        );
  $row=$this->db->get_where('rp1',$where)->row_array();
  extract($row);
}
else{
  $parameter='simpan';
}
?>
<form class="validate form-horizontal" id="form-rp1" method="POST" action="<?=site_url()?>rp1/<?=$parameter;?>">
<div class="col-lg-12">
	<div class="form-group">
		<label>No Urut</label>
		<?=input_text('no_rp1',($no_rp1??''),'md-input','required');?>
		<?=input_hidden('def_no_rp1',($no_rp1??''),'md-input','required');?>
</div>
</div>
<div class="col-lg-12">
	<div class="row">
	<div class="col-lg-6">
		<div class="form-group">
			<label>Nama Penerima</label>
			<?=input_text('nama_penerima',($nama_penerima??''),'md-input','required');?>
		</div>
	</div>
							
	<div class="col-lg-6">
		<div class="form-group">
			<label>Tanggal Penerimaan Laporan</label>
			<?=input_date('tgl_penerimaan_laporan',($tgl_penerimaan_laporan??''),'md-input','required');?>
		</div>
	</div>
	</div>
</div>
						
<div class="col-lg-12">
	<div class="form-group">
		<label>Identitas Terlapor/calon Tersangka</label>
		<?php 
			$op=NULL;
			$op['']='Pilih Salah Satu';  
			$this->db->order_by('nama_lengkap','ASC');
			$data=$this->db->get('biodata');
			foreach($data->result() as $row){
				$op[$row->nik]=$row->nama_lengkap .' ('.$row->nik.')';
			}
			echo select('nik',$op,($nik??''),'select2','required');?>
	</div>
</div>
						
<div class="col-lg-12">
	<div class="form-group">
		<label>Isi Ringkas Laporan/Kejadian</label>
		<?=textarea('isi_ringkas',($isi_ringkas??''),'md-input','required');?>
	</div>
</div>
						
<div class="col-lg-12">
	<div class="form-group">
		<label>Diteruskan/Petunjuk Pimpinan</label>
		<?=input_text('diteruskan',($diteruskan??''),'md-input','required');?>
	</div>
</div>
						
<div class="col-lg-12">
	<div class="form-group">
		<label>Keterangan</label>
		<?=textarea('keterangan',($keterangan??''),'md-input','required');?>
	</div>
</div>
<?=input_hidden('tgl_simpan',($tgl_simpan??''),'','required');?><?=input_hidden('no_agenda',($no_agenda??''),'','required');?><!--endform-->
  <div class="col-lg-12">
    <div class="form-group">
      <label></label>
      <button type="submit" name="simpan" class="btn btn-primary" value="true">Simpan</button>
      <a href="javascript:history.go(-1)" class="btn btn-danger">Batal</a>
    </div>
  </div>
  <div class="clearfix"></div>
  </div>
</form>
  	</div>
</div>

  <?php } else { ?>
<!--OPEN TABLE-->
<div class="card">
	<div class="card-header">
		<h3 class="card-title">DATA - Register <?=rpConvert('1',1)?></h3>

		<div class="card-tools">
		<button type="button" class="btn btn-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
			<i class="fa fa-minus"></i></button>
		</div>
	</div>
	<div class="card-body">

					<!-- table -->
		<?php echo anchor(site_url('rp1?tambah'),"<i class='fa fa-plus'></i> Tambah",['class'=>'btn btn-success btn-tambah']);?>
					
					<hr>
					<?php echo $this->session->flashdata('info');?>
					<?php echo $table;?>
					<!-- end table -->
	</div>
</div>

  <?php }

