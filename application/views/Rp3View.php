<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
  <?php if(isset($_GET['tambah']) OR isset($_GET['ubah'])){ ?>    <!--OPEN FORM -->
  <div class="card">
	<div class="card-header">
		<h3 class="card-title">FORM RP3</h3>

		<div class="card-tools">
		<button type="button" class="btn btn-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
			<i class="fa fa-minus"></i></button>
		</div>
	</div>
	<div class="card-body">

	<!-- form -->
<?php
if(isset($_GET['ubah'])){
  $parameter='ubah';
  $where=array(
          	'no_rp3'=>$_GET['no_rp3']
	        );
  $row=$this->db->get_where('rp3',$where)->row_array();
  extract($row);
}
else{
  $parameter='simpan';
}
?>
<form class="validate form-horizontal" id="form-rp3" method="POST" action="<?=site_url()?>rp3/<?=$parameter;?>">
<div class="col-md-12">
	<div class="row">
		<div class="col-lg-6">
			<div class="form-group">
				<label>No Urut Register</label>
				<?=input_text('no_rp3',($no_rp3??''),'md-input','required');?>
			</div>
		</div>
								
		<div class="col-lg-6">
			<div class="form-group">
				<label>Tanggal Register Perkara</label>
				<?=input_date('tgl_reg_perkara',($tgl_reg_perkara??''),'md-input','required');?>
			</div>
		</div>
	</div>
</div>				
<div class="col-lg-12">
	<div class="form-group">
		<label>Asal Perkara</label>
		<?=input_text('asal_perkara',($asal_perkara??''),'md-input','required');?>
	</div>
</div>
						
<div class="col-lg-12">
	<div class="form-group">
		<label>Identitas Tersangka</label>
		<?php 
			$op=NULL;
			$op['']='Pilih Salah Satu';  
			$this->db->order_by('nama_lengkap','ASC');
			$data=$this->db->get('biodata');
			foreach($data->result() as $row){
				$op[$row->nik]=$row->nama_lengkap;
			}
			echo select('nik',$op,($nik??''),'select2','required');?>
	</div>
</div>
						
<div class="col-lg-12">
	<div class="form-group">
		<label>Kasus Posisi/Pasal Yang Disangkakan</label>
		<?=textarea('kasus',($kasus??''),'md-input','required');?>
	</div>
</div>
						
<div class="col-lg-12">
	<div class="form-group">
		<label>Status Tersangka/SP Penahanan</label>
		<?=input_text('status_tsk',($status_tsk??''),'md-input','required');?>
	</div>
</div>
<div class="col-md-12">
	<div class="row">
					
		<div class="col-lg-6">
			<div class="form-group">
				<label>No SP Penahanan</label>
				<?=input_text('no_sp_penahanan',($no_sp_penahanan??''),'md-input','required');?>
			</div>
		</div>
								
		<div class="col-lg-6">
			<div class="form-group">
				<label>Tanggal</label>
				<?=input_date('tgl_sp_penahanan',($tgl_sp_penahanan??''),'md-input','required');?>
			</div>
		</div>
		</div>
</div>					
<div class="col-lg-12">
	<div class="form-group">
		<label>Nomor Denda Sitaan</label>
		<?=input_text('no_denda_sitaan',($no_denda_sitaan??''),'md-input','required');?>
	</div>
</div>
<div class="col-lg-12">
	<div class="form-group">
		<label>Tanggal Denda Sitaan</label>
		<?=input_date('tgl_denda_sitaan',($tgl_denda_sitaan??''),'md-input','required');?>
	</div>
</div>
						
<div class="col-lg-12">
	<div class="form-group">
		<label>Jaksa Penyidik</label>
		<?php 
			$op=NULL;
			$op['']='Pilih Salah Satu';  
			$this->db->order_by('nip','ASC');
			$data=$this->db->get('pegawai');
			foreach($data->result() as $row){
				$op[$row->id_pegawai]=$row->nip;
			}
			echo select('id_pegawai',$op,($id_pegawai??''),'','required');?>
	</div>
</div>
			

<div class="col-md-12">
	<div class="row">			
		<div class="col-lg-6">
			<div class="form-group">
				<label>Nomor Penghentian DIK</label>
				<?=input_text('no_penghentian_dik',($no_penghentian_dik??''),'md-input','required');?>
			</div>
		</div>
								
		<div class="col-lg-6">
			<div class="form-group">
				<label>Tanggal</label>
				<?=input_date('tgl_penghentian_dik',($tgl_penghentian_dik??''),'md-input','required');?>
			</div>
		</div>
				
	</div>
</div>					
<div class="col-lg-12">
	<div class="form-group">
		<label>Pengalihan Ke Instansi Lain</label>
		<?=input_text('pengalihan',($pengalihan??''),'md-input','required');?>
	</div>
</div>

<div class="col-md-12">
	<div class="row">
		<div class="col-lg-6">
			<div class="form-group">
				<label>Nomor Pelimpahan Tut</label>
				<?=input_text('no_pelimpahan',($no_pelimpahan??''),'md-input','required');?>
			</div>
		</div>
								
		<div class="col-lg-6">
			<div class="form-group">
				<label>Tgl</label>
				<?=input_date('tgl_pelimpahan',($tgl_pelimpahan??''),'md-input','required');?>
			</div>
		</div>
	</div>
</div>					
<div class="col-lg-12">
	<div class="form-group">
		<label>Keterangan</label>
		<?=textarea('keterangan',($keterangan??''),'md-input','required');?>
	</div>
</div>
<?=input_hidden('tgl_simpan',($tgl_simpan??''),'','required');?><?=input_hidden('no_agenda',($no_agenda??''),'','required');?><!--endform-->
  <div class="col-lg-12">
    <div class="form-group">
      <label></label>
      <button type="submit" name="simpan" class="btn btn-primary" value="true">Simpan</button>
      <a href="javascript:history.go(-1)" class="btn btn-danger">Batal</a>
    </div>
  </div>
  <div class="clearfix"></div>
  </div>
</form>
  	</div>
</div>

  <?php } else { ?>
<!--OPEN TABLE-->
<div class="card">
	<div class="card-header">
		<h3 class="card-title">DATA RP3</h3>

		<div class="card-tools">
		<button type="button" class="btn btn-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
			<i class="fa fa-minus"></i></button>
		</div>
	</div>
	<div class="card-body">

					<!-- table -->
		<?php echo anchor(site_url('rp3?tambah'),"<i class='fa fa-plus'></i> Tambah",['class'=>'btn btn-success btn-tambah']);?>
					
					<hr>
					<?php echo $this->session->flashdata('info');?>
					<?php echo $table;?>
					<!-- end table -->
	</div>
</div>

  <?php }

