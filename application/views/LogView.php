<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>APPCFARM | Login</title>
  <?php include('templated/head.php')?>
  <style>
    body{
      background:url(<?=base_url('assets/images/confectionary.png')?>) !important;
      background-blend-mode: difference !important;
      background-size: cover;
    }
    .login-box, .register-box {
      width: 360px;
      margin: 5% auto;
  }
body:before{
  width: 100%;
  height: 100vh;
  display: block;
  position: fixed;
  top:0;
   content: '';
     z-index: -1;
  background: rgba(255,255,255,.4);
}

  </style>
</head>
<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
  <div class="">
    <img src="<?=base_url('assets/images/logo.png')?>" alt="" width="100" height="100" class="img-circle">
  </div>
    <a><b style="color: #ff9103;;text-shadow: 1px 1px 1px #fff;font-weight: bold">REGISTER TIPIDSUS</b> </a>
  </div>
  <!-- /.login-logo -->
  <div class="card">
    <div class="card-body login-card-body">
      <p class="login-box-msg">Sign in to start your session</p>

	  <form action="<?=site_url('log/check')?>" method="post">
		<?php echo $this->session->flashdata('info');?>
	  
        <div class="form-group has-feedback">
			<label for="">Username</label>
          <input type="text" class="form-control" name="username" placeholder="Username">
        </div>
        <div class="form-group has-feedback">
			<label for="">Password</label>
          <input type="password" class="form-control" name="password" placeholder="Password">
        </div>
        <div class="row">
          <div class="col-4">
            <button type="submit" class="btn btn-primary btn-block btn-flat"><i class="fa fa-sign-in"></i> Masuk</button>
          </div>
          <!-- /.col -->
        </div>
      </form>

    </div>
    <!-- /.login-card-body -->
  </div>
</div>
<!-- /.login-box -->
<?php include('templated/js.php')?>

<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass   : 'iradio_square-blue',
      increaseArea : '20%' // optional
    })
  })
</script>
</body>
</html>
