<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
	<!-- form -->
<!-- The Modal -->
<div class="modal fade" id="FormModal">
<form class="validate form-horizontal" id="form-pekerjaan" method="POST">
  <div class="modal-dialog">
    <div class="modal-content">
      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Form Data Pekerjaan</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <!-- Modal body -->
      <div class="modal-body">
        <?=input_hidden('id_pekerjaan',($id_pekerjaan??''),'','required');?>						
<div class="col-lg-12">
	<div class="form-group">
		<label>Nama Pekerjaan</label>
		<?=input_text('nama_pekerjaan',($nama_pekerjaan??''),'md-input','required');?>
	</div>
</div>
<!--endform-->
      </div>
       <!-- Modal footer -->
      <div class="modal-footer">
         <button type="submit" name="simpan" class="btn btn-primary" value="true">Simpan</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal">Tutup</button>
      </div>
    </div>
  </div>
</form>
</div><!--OPEN TABLE-->
<div class="card">
	<div class="card-header">
		<h3 class="card-title">DATA DATA PEKERJAAN</h3>

		<div class="card-tools">
		<button type="button" class="btn btn-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
			<i class="fa fa-minus"></i></button>
		</div>
	</div>
	<div class="card-body">

					<!-- table -->
					<button type="button" class="btn btn-success btn-tambah" data-toggle="modal" data-target="#FormModal">
						<i class='fa fa-plus'></i> Tambah
					</button>					
					<hr>
					<?php echo $this->session->flashdata('info');?>
					<?php echo $table;?>
					<!-- end table -->
	</div>
</div>


