<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
  <?php if(isset($_GET['tambah']) OR isset($_GET['ubah'])){ ?>    <!--OPEN FORM -->
  <div class="card">
	<div class="card-header">
		<h3 class="card-title">FORM RP2</h3>

		<div class="card-tools">
		<button type="button" class="btn btn-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
			<i class="fa fa-minus"></i></button>
		</div>
	</div>
	<div class="card-body">

	<!-- form -->
<?php
if(isset($_GET['ubah'])){
  $parameter='ubah';
  $where=array(
          	'no_rp2'=>$_GET['no_rp2']
	        );
  $row=$this->db->get_where('rp2',$where)->row_array();
  extract($row);
}
else{
  $parameter='simpan';
}
?>
<form class="validate form-horizontal" id="form-rp2" method="POST" action="<?=site_url()?>rp2/<?=$parameter;?>">
  
<div class="col-lg-12">
	<div class="form-group">
		<label>No Urut Register</label>
		<?=input_text('no_rp2',($no_rp2??''),'md-input','required');?>
	</div>
</div>
						
<div class="col-lg-12">
	<div class="form-group">
		<label>Tanggal Penyelidikan</label>
		<?=input_date('tgl_penyelidikan',($tgl_penyelidikan??''),'md-input','required');?>
	</div>
</div>
						
<div class="col-lg-12">
	<div class="form-group">
		<label>Kasus/Masalah</label>
		<?=input_text('kasus',($kasus??''),'md-input','required');?>
	</div>
</div>
						
<div class="col-lg-12">
	<div class="form-group">
		<label>Jaksa Penyidik</label>
		<?php 
			$op=NULL;
			$op['']='Pilih Salah Satu';  
			$this->db->order_by('nip','ASC');
			$data=$this->db->get('pegawai');
			foreach($data->result() as $row){
				$op[$row->id_pegawai]=$row->nip;
			}
			echo select('pegawai',$op,($pegawai??''),'','required');?>
	</div>
</div>
						
<div class="col-lg-12">
	<div class="form-group">
		<label>Nama Pelapor/sumber  Laporan, Tanggal Laporan</label>
		<?=textarea('nama_pelapor',($nama_pelapor??''),'md-input','required');?>
	</div>
</div>
						
<div class="col-lg-12">
	<div class="form-group">
		<label>Identitas Terlapor/instansi/badan Organisasi Yang Menjadi Obyek LID</label>
		<?=textarea('identitas',($identitas??''),'md-input','required');?>
	</div>
</div>
						
<div class="col-lg-12">
	<div class="form-group">
		<label>Ditutup Tanggal</label>
		<?=input_date('tgl_ditutup',($tgl_ditutup??''),'md-input','required');?>
	</div>
</div>
						
<div class="col-lg-12">
	<div class="form-group">
		<label>Ditingkatkan Ke DIK Tanggal</label>
		<?=input_date('tgl_ditingkatkan',($tgl_ditingkatkan??''),'md-input','required');?>
	</div>
</div>
<?=input_hidden('keterangan',($keterangan??''),'','required');?><?=input_hidden('tgl_simpan',($tgl_simpan??''),'','required');?><?=input_hidden('no_agenda',($no_agenda??''),'','required');?><!--endform-->
  <div class="col-lg-12">
    <div class="form-group">
      <label></label>
      <button type="submit" name="simpan" class="btn btn-primary" value="true">Simpan</button>
      <a href="javascript:history.go(-1)" class="btn btn-danger">Batal</a>
    </div>
  </div>
  <div class="clearfix"></div>
  </div>
</form>
  	</div>
</div>

  <?php } else { ?>
<!--OPEN TABLE-->
<div class="card">
	<div class="card-header">
		<h3 class="card-title">DATA RP2</h3>

		<div class="card-tools">
		<button type="button" class="btn btn-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
			<i class="fa fa-minus"></i></button>
		</div>
	</div>
	<div class="card-body">

					<!-- table -->
		<?php echo anchor(site_url('rp2?tambah'),"<i class='fa fa-plus'></i> Tambah",['class'=>'btn btn-success btn-tambah']);?>
					
					<hr>
					<?php echo $this->session->flashdata('info');?>
					<?php echo $table;?>
					<!-- end table -->
	</div>
</div>

  <?php }

