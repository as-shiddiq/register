<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
  <?php if(isset($_GET['tambah']) OR isset($_GET['ubah'])){ ?>    <!--OPEN FORM -->
  <div class="card">
	<div class="card-header">
		<h3 class="card-title">FORM DATA BIODATA</h3>

		<div class="card-tools">
		<button type="button" class="btn btn-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
			<i class="fa fa-minus"></i></button>
		</div>
	</div>
	<div class="card-body">

	<!-- form -->
<?php
if(isset($_GET['ubah'])){
  $parameter='ubah';
  $where=array(
          	'nik'=>$_GET['nik']
	        );
  $row=$this->db->get_where('biodata',$where)->row_array();
  extract($row);
}
else{
  $parameter='simpan';
}
?>
<form class="validate form-horizontal" id="form-biodata" method="POST" action="<?=site_url()?>biodata/<?=$parameter;?>">
  <?=input_hidden('def_nik',($nik??''),'','required');?>

  <div class="row">
	<div class="col-md-6">
	<!-- section1	 -->
		<div class="col-lg-12">
			<div class="form-group">
				<label>NIK</label>
				<?=input_text('nik',($nik??''),'md-input','required');?>
			</div>
		</div>
		<div class="col-lg-12">
			<div class="form-group">
				<label>Nama Lengkap</label>
				<?=input_text('nama_lengkap',($nama_lengkap??''),'md-input','required');?>
			</div>
		</div>
								
		<div class="col-lg-12">
			<div class="form-group">
				<label>Tmpt Lahir</label>
				<?=input_text('tmpt_lahir',($tmpt_lahir??''),'md-input','required');?>
			</div>
		</div>
								
		<div class="col-lg-12">
			<div class="form-group">
				<label>Tgl Lahir</label>
				<?=input_date('tgl_lahir',($tgl_lahir??''),'md-input','required');?>
			</div>
		</div>
								
		<div class="col-lg-12">
			<div class="form-group">
				<label>Jk</label>
				<?php 
					$op=NULL;
					$op['']='Pilih Salah Satu';  
					$op['Laki-Laki']='Laki-Laki';
					$op['Perempuan']='Perempuan';
					echo select('jk',$op,($jk??''),'','required');?>
			</div>
		</div>
				
		<div class="col-lg-12">
			<div class="form-group">
				<label>Tmpt Tinggal</label>
				<?=textarea('tmpt_tinggal',($tmpt_tinggal??''),'md-input','required');?>
			</div>
		</div>
	<!-- section1	 -->
	</div>
	<div class="col-md-6">
	<!-- section2 -->
								
		<div class="col-lg-12">
			<div class="form-group">
				<label>Pendidikan</label>
				<?php 
					$op=NULL;
					$op['']='Pilih Salah Satu';  
					$this->db->order_by('id_pendidikan','ASC');
					$data=$this->db->get('pendidikan');
					foreach($data->result() as $row){
						$op[$row->id_pendidikan]=$row->nama_pendidikan;
					}
					echo select('id_pendidikan',$op,($id_pendidikan??''),'','required');?>
			</div>
		</div>
								
		<div class="col-lg-12">
			<div class="form-group">
				<label>Pekerjaan</label>
				<?php 
					$op=NULL;
					$op['']='Pilih Salah Satu';  
					$this->db->order_by('nama_pekerjaan','ASC');
					$data=$this->db->get('pekerjaan');
					foreach($data->result() as $row){
						$op[$row->id_pekerjaan]=$row->nama_pekerjaan;
					}
					echo select('id_pekerjaan',$op,($id_pekerjaan??''),'','required');?>
			</div>
		</div>
								
		<div class="col-lg-12">
			<div class="form-group">
				<label>Kewarganegaraan</label>
				<?=input_text('kewarganegaraan',($kewarganegaraan??''),'md-input','required');?>
			</div>
		</div>
						
								
		<div class="col-lg-12">
			<div class="form-group">
				<label>Agama</label>
				<?php 
					$op=NULL;
					$op['']='Pilih Salah Satu';  
					$op['Islam']='Islam';
					$op['Kristen']='Kristen';
					$op['Hindu']='Hindu';
					$op['Budha']='Budha';
					echo select('agama',$op,($agama??''),'','required');?>
			</div>
		</div>
	<!-- section2 -->
	</div>
  </div>						

<?=input_hidden('created_at',($created_at??''),'','required');?><!--endform-->
  <div class="col-lg-12">
    <div class="form-group">
      <label></label>
      <button type="submit" name="simpan" class="btn btn-primary" value="true">Simpan</button>
      <a href="javascript:history.go(-1)" class="btn btn-danger">Batal</a>
    </div>
  </div>
  <div class="clearfix"></div>
  </div>
</form>
  	</div>
</div>

  <?php } else { ?>
<!--OPEN TABLE-->
<div class="card">
	<div class="card-header">
		<h3 class="card-title">DATA DATA BIODATA</h3>

		<div class="card-tools">
		<button type="button" class="btn btn-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
			<i class="fa fa-minus"></i></button>
		</div>
	</div>
	<div class="card-body">

					<!-- table -->
		<?php echo anchor(site_url('biodata?tambah'),"<i class='fa fa-plus'></i> Tambah",['class'=>'btn btn-success btn-tambah']);?>
					
					<hr>
					<?php echo $this->session->flashdata('info');?>
					<?php echo $table;?>
					<!-- end table -->
	</div>
</div>

  <?php }

