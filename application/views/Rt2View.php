<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
  <?php if(isset($_GET['tambah']) OR isset($_GET['ubah'])){ ?>    <!--OPEN FORM -->
  <div class="card">
	<div class="card-header">
		<h3 class="card-title">FORM RT2</h3>

		<div class="card-tools">
		<button type="button" class="btn btn-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
			<i class="fa fa-minus"></i></button>
		</div>
	</div>
	<div class="card-body">

	<!-- form -->
<?php
if(isset($_GET['ubah'])){
  $parameter='ubah';
  $where=array(
          	'no_rt2'=>$_GET['no_rt2']
	        );
  $row=$this->db->get_where('rt2',$where)->row_array();
  extract($row);
}
else{
  $parameter='simpan';
}
?>
<form class="validate form-horizontal" id="form-rt2" method="POST" action="<?=site_url()?>rt2/<?=$parameter;?>">

<div class="col-lg-12">
	<div class="form-group">
		<label>No Urut Register</label>
		<?=input_text('no_rt2',($no_rt2??''),'md-input','required');?>
	</div>
</div>
<div class="col-md-12">
	<div class="row">
							
	<div class="col-lg-12">
		<div class="form-group">
			<label>No.</label>
			<?=input_text('no_perpanjangan',($no_perpanjangan??''),'md-input','required');?>
		</div>
	</div>
							
	<div class="col-lg-12">
		<div class="form-group">
			<label>Tanggal</label>
			<?=input_date('tgl_ketetapan_perpanjangan',($tgl_ketetapan_perpanjangan??''),'md-input','required');?>
		</div>
	</div>
	</div>
</div>					
<div class="col-lg-12">
	<div class="form-group">
		<label>Tersangka Nama Lengkap, Tempat Lahir, Umur/tgl. Lahir, Jenis Kelamin, Kebangsaan/kewarganegaraan, Tempat Tinggal, Agama, Pekerjaan, Pendidikan</label>
		<?php 
			$op=NULL;
			$op['']='Pilih Salah Satu';  
			$this->db->order_by('nama_lengkap','ASC');
			$data=$this->db->get('biodata');
			foreach($data->result() as $row){
				$op[$row->nik]=$row->nama_lengkap;
			}
			echo select('nik',$op,($nik??''),'','required');?>
	</div>
</div>
<div class="col-lg-12">
	<div class="form-group">
		<label>Pasal Yang Disangkakan</label>
		<?=input_text('pasal_yg_disangkakan',($pasal_yg_disangkakan??''),'md-input','required');?>
	</div>
</div>
<div class="col-md-12">
	<div class="row">
		<div class="col-lg-6">
			<div class="form-group">
				<label>Nomor/Tanggal</label>
				<?=input_text('nomor',($nomor??''),'md-input','required');?>
			</div>
		</div>
								
		<div class="col-lg-6">
			<div class="form-group">
				<label>Tgl</label>
				<?=input_date('tgl',($tgl??''),'md-input','required');?>
			</div>
		</div>
		</div>
</div>				
	<div class="col-lg-12">
	<div class="form-group">
		<label>Tanggal Mulai Ditahan</label>
		<?=input_date('tgl_ditahan',($tgl_ditahan??''),'md-input','required');?>
	</div>
</div>
		
<div class="col-md-12">
	<div class="row">				
		<div class="col-lg-6">
			<div class="form-group">
				<label>Perpanjangan Penahanan Dari Penuntut Umum Mulai Tgl.</label>
				<?=input_date('tgl_perpanjangan',($tgl_perpanjangan??''),'md-input','required');?>
			</div>
		</div>
								
		<div class="col-lg-6">
			<div class="form-group">
				<label>Sampai</label>
				<?=input_date('tgl_perpanjangan2',($tgl_perpanjangan2??''),'md-input','required');?>
			</div>
		</div>
	</div>
</div>	
						
<div class="col-lg-12">
	<div class="form-group">
		<label>Ditangguhkan Penahanannya Oleh Penyidik Mulai Tanggal</label>
		<?=input_text('ditangguhkan',($ditangguhkan??''),'md-input','required');?>
	</div>
</div>
						
<div class="col-lg-12">
	<div class="form-group">
		<label>Keterangan</label>
		<?=textarea('keterangan',($keterangan??''),'md-input','required');?>
	</div>
</div>
<?=input_hidden('tgl_simpan',($tgl_simpan??''),'','required');?><!--endform-->
  <div class="col-lg-12">
    <div class="form-group">
      <label></label>
      <button type="submit" name="simpan" class="btn btn-primary" value="true">Simpan</button>
      <a href="javascript:history.go(-1)" class="btn btn-danger">Batal</a>
    </div>
  </div>
  <div class="clearfix"></div>
  </div>
</form>
  	</div>
</div>

  <?php } else { ?>
<!--OPEN TABLE-->
<div class="card">
	<div class="card-header">
		<h3 class="card-title">DATA RT2</h3>

		<div class="card-tools">
		<button type="button" class="btn btn-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
			<i class="fa fa-minus"></i></button>
		</div>
	</div>
	<div class="card-body">

					<!-- table -->
		<?php echo anchor(site_url('rt2?tambah'),"<i class='fa fa-plus'></i> Tambah",['class'=>'btn btn-success btn-tambah']);?>
					
					<hr>
					<?php echo $this->session->flashdata('info');?>
					<div style="width:100%;overflow:scroll">
						<?php echo $table;?>
					</div>
					<!-- end table -->
	</div>
</div>

  <?php }

