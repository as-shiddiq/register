<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php include('home/counter.php')?>
 <div class="card">
	<div class="card-header">
		<h3 class="card-title">
            Grafik Register Perkara bulan <?=bulan_huruf(date('m'))?> <?=date('Y')?>
		</h3>

		<div class="card-tools">
		<button type="button" class="btn btn-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
			<i class="fa fa-minus"></i></button>
		</div>
	</div>
	<div class="card-body">
			<div id="container-akses" style="height:500px">
            </div>

            <table id="table-akses" class="table" style="display:none">
            	<thead>
            		<tr>
            			<td></td>
            			<td>Total</td>
            		</tr>
            	</thead>
            	<tbody>
            	 <?php
            	 $j=0;
                for ($i=1; $i < 15; $i++) { 
                	if($i%2==0){
	                  	$j=$i%3;
                	}
                	else{
	                  	$j=$i%5;
                	}
                  ?>
                  <tr>
                  	<th>
                      <?=rpConvert($i,0).' ('.rpConvert($i,1).')'?>
                  	</th>
                  	<td><?=(($i*2)-3*$j)?></td>
                  </tr>
                  <?php
                }
              ?>
            	</tbody>

            </table>
	</div>
</div>