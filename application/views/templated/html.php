<!DOCTYPE html>
<html>
  <head>
    <title><?php echo $title?></title>
    <?php include 'head.php';?>
  </head>
  <body class="sidebar-mini sidebar-collapse">
    <div class="wrapper">
      <?php include 'navbar.php';?>
      <?php include 'aside.php';?>
      <div class="content-wrapper">
        <div class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1 class="m-0 text-dark" style="font-family: 'Lobster', cursive;"><?=ucfirst(namePage($this->uri->segment(1)))?></h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="<?=site_url()?>">Beranda</a></li>
                <li class="breadcrumb-item active"><?=ucfirst(namePage($this->uri->segment(1)))?></li>
              </ol>
            </div><!-- /.col -->
          </div><!-- /.row -->
        </div><!-- /.container-fluid -->
      </div>
      <section class="content">
        <?php echo $body;?>
      </section>
      </div>
      
      <?php include 'footer.php'?>
      <?php include 'js.php'?>
      <?php
        if(isset($js)){
          echo $js;
        }
      ?>
  </body>
</html>
