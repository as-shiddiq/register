<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<!-- Tell the browser to be responsive to screen width -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- Font Awesome -->
<link rel="stylesheet" href="<?=templates()?>plugins/font-awesome/css/font-awesome.min.css">
<!-- Ionicons -->
<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
<!-- Theme style -->
<link rel="stylesheet" href="<?=templates()?>dist/css/adminlte.min.css">
<!-- iCheck -->
<link rel="stylesheet" href="<?=templates()?>plugins/iCheck/flat/blue.css">
<!-- Morris chart -->
<link rel="stylesheet" href="<?=templates()?>plugins/morris/morris.css">
<!-- jvectormap -->
<link rel="stylesheet" href="<?=templates()?>plugins/jvectormap/jquery-jvectormap-1.2.2.css">
<!-- Date Picker -->
<link rel="stylesheet" href="<?=templates()?>plugins/datepicker/datepicker3.css">
<!-- Daterange picker -->
<link rel="stylesheet" href="<?=templates()?>plugins/daterangepicker/daterangepicker-bs3.css">
<!-- bootstrap wysihtml5 - text editor -->
<link rel="stylesheet" href="<?=templates()?>plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
<link rel="stylesheet" href="<?=templates()?>plugins/select2/select2.css">
<!-- Google Font: Source Sans Pro -->
<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

  <link rel="stylesheet" href="<?=templates()?>plugins/datatables/dataTables.bootstrap4.min.css">
<style>
/* latin */
@font-face {
  font-family: 'Lobster';
  font-style: normal;
  font-weight: 400;
  src: url(<?=base_url('assets/fonts/Lobster/Lobster-Regular.ttf')?>);
}
.btn-group-xs>.btn, .btn-xs {
    padding: .25rem .5rem;
    font-size: .875rem;
    line-height: 1.5;
    border-radius: .2rem;
}
body{
  font-size:13px
}

  .select2-container--default .select2-selection--multiple .select2-selection__choice,
  .select2-container--default .select2-selection--single{
    border-radius: 0 ;
    border-color:#ddd;
    height: 38px;
    padding: 6px 4px;
  }
  .select2 .selection ,.select2{
    width: 100% !important;
  }
  .select2 .selection span{
    font-size: 14px !important
  }
  .select2-results{
    background: #fff
  }
  input,textarea,select,.btn{
    border-radius:0 !important
  }
</style>