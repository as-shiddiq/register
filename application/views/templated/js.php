<!-- jQuery -->
<script src="<?=templates()?>plugins/jquery/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->
<script src="<?=templates()?>plugins/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- DataTables -->
<script src="<?=base_url('assets/jquery-datatable/jquery.dataTables.js')?>"></script>

<script src="<?=templates()?>/plugins/datatables/dataTables.bootstrap4.min.js"></script>
<script src="<?=templates()?>plugins/sparkline/jquery.sparkline.min.js"></script>
<!-- jQuery Knob Chart -->
<script src="<?=templates()?>plugins/knob/jquery.knob.js"></script>
<!-- daterangepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
<script src="<?=templates()?>plugins/daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="<?=templates()?>plugins/datepicker/bootstrap-datepicker.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="<?=templates()?>plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="<?=templates()?>plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?=templates()?>plugins/fastclick/fastclick.js"></script>
<script src="<?=templates()?>plugins/select2/select2.js"></script>
<!-- AdminLTE App -->
<script src="<?=templates()?>dist/js/adminlte.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="<?=templates()?>dist/js/pages/dashboard.js"></script>

<!-- AdminLTE for demo purposes -->
<script>
$(".select2").select2();
  $(function () {
    $('#table-dt').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
  });
  $(".pilih-kontrak").change(function(){
    if($(this).val()!=''){
      $(".aktifkan-kontrak").submit();
    }
  })
</script>