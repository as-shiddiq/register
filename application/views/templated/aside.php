
  <!-- Main Sidebar Container -->
  <aside class="main-sidebar elevation-1 sidebar-light-warning">
    <!-- Brand Logo -->
    <a href="index3.html" class="brand-link bg-warning">
      <img src="<?=base_url('assets/images/logo.png')?>" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light"><b>REGISTER</b></span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item has-treeview menu-open">
            <a href="<?=site_url()?>" class="nav-link active">
              <i class="nav-icon fa fa-home"></i>
              <p>
                  Beranda
              </p>
            </a>
          </li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fa fa-window-restore"></i>
              <p>
                Master Data
                <i class="right fa fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="<?=site_url('pendidikan')?>" class="nav-link">
                  <i class="fa fa-circle-o nav-icon"></i>
                  <p>Pendidikan</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?=site_url('pekerjaan')?>" class="nav-link">
                  <i class="fa fa-circle-o nav-icon"></i>
                  <p>Pekerjaan</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?=site_url('pangkatgol')?>" class="nav-link">
                  <i class="fa fa-circle-o nav-icon"></i>
                  <p>Pangkat/Golongan</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?=site_url('pegawai')?>" class="nav-link">
                  <i class="fa fa-circle-o nav-icon"></i>
                  <p>Pegawai</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?=site_url('user')?>" class="nav-link">
                  <i class="fa fa-circle-o nav-icon"></i>
                  <p>Pengguna</p>
                </a>
              </li>
            </ul>
          </li>
          
          <li class="nav-item">
            <a href="<?=site_url('biodata')?>" class="nav-link">
              <i class="nav-icon fa fa-users"></i>
              <p>
                  Biodata
              </p>
            </a>
          </li>

          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fa fa-pencil"></i>
              <p>
                Register Benda
                <i class="right fa fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
               <?php
                for ($i=1; $i < 3; $i++) { 
                  ?>
                  <li class="nav-item">
                    <a href="<?=site_url('rb'.$i)?>" class="nav-link">
                      <i class="fa fa-circle-o nav-icon"></i>
                      <p><?=rbConvert($i,0).' ('.rbConvert($i,1).')'?></p>
                    </a>
                  </li>
                  <?php
                }
              ?>
            </ul>
          </li>
          <li class="nav-item has-treeview <?=($this->session->userdata('level')=='pemasok')?'menu-open':''?>">
            <a href="#" class="nav-link">
              <i class="nav-icon fa fa-pencil"></i>
              <p>
                  Register Perkara
                <i class="right fa fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
            <?php
                for ($i=1; $i < 15; $i++) { 
                  ?>
                  <li class="nav-item">
                    <a href="<?=site_url('rp'.$i)?>" class="nav-link">
                      <i class="fa fa-circle-o nav-icon"></i>
                      <p><?=rpConvert($i,0).' ('.rpConvert($i,1).')'?></p>
                    </a>
                  </li>
                  <?php
                }
              ?>
            </ul>
          </li>
          <li class="nav-item has-treeview <?=($this->session->userdata('level')=='pemasok')?'menu-open':''?>">
            <a href="#" class="nav-link">
              <i class="nav-icon fa fa-pencil"></i>
              <p>
                  Register Tahanan
                <i class="right fa fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
            <?php
                for ($i=1; $i < 4; $i++) { 
                  ?>
                  <li class="nav-item">
                    <a href="<?=site_url('rt'.$i)?>" class="nav-link">
                      <i class="fa fa-circle-o nav-icon"></i>
                      <p><?=rtConvert($i,0).' ('.rtConvert($i,1).')'?></p>
                    </a>
                  </li>
                  <?php
                }
              ?>
            </ul>
          </li>
          
          
          <!-- <li class="nav-item has-treeview">
            <a href="<?=site_url('laporan')?>" class="nav-link">
              <i class="nav-icon fa fa-newspaper-o"></i>
              <p>
                Laporan
              </p>
            </a>
            
          </li> -->
          
          <li class="nav-item">
            <a href="<?=site_url('log/out')?>" class="col-danger nav-link" style="color:red" onclick="return confirm('Keluar?')">
              <i class="nav-icon fa fa-power-off"></i>
              <p>
                  Keluar
              </p>
            </a>
          </li>
          
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>
