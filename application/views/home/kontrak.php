
<div class="col-md-12">
<div class="card">
    <div class="card-header">   
	<h3 class="card-title">Kontrak yang masih berjalan</h3>
	
	<div class="card-tools">
		<button type="button" class="btn btn-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
			<i class="fa fa-minus"></i></button>
    </div>
    </div>
		
    <div class="card-body">
        <?php
        $this->db->where('selesai','T');
        $get_data=$this->kontrakModel->get_data();
        $template = array(
            'table_open' => '<table border="0" cellpadding="4" cellspacing="0" class="table table-bordered dt-responsive display"',
        );
        $this->table->set_template($template);
        $this->table->set_heading('No','nama mitra','no kontrak','tgl kontrak','');
        $i=1;
        foreach($get_data->result() as $row){
            $this->table->add_row(array("data"=>$i,"width"=>"50px","align"=>"center"),
                                                        $row->nama_mitra,
                                                        $row->no_kontrak,
                                                        standar_tanggal($row->tgl_kontrak),
                                                        array("data"=>anchor(site_url("kontrak/detail?id_kontrak=".$row->id_kontrak),"<i class='fa fa-eye'></i> Detail",["class"=>"btn btn-xs btn-warning","onclick"=>false]),"width"=>"20px","align"=>"center"));
            $i++;
        }
        echo $this->table->generate();
    ?>
    </div>
</div>
</div>