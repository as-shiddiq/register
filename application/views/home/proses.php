
<div class="col-md-6">
<div class="card">
    <div class="card-header">   
	<h3 class="card-title">Pengiriman yang diproses</h3>
	
	<div class="card-tools">
		<button type="button" class="btn btn-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
			<i class="fa fa-minus"></i></button>
    </div>
    </div>
		
    <div class="card-body">
        <?php
        $this->db->where('a.status','proses');
        $get_data=$this->kirimitemModel->get_data();
        $template = array(
            'table_open' => '<table border="0" cellpadding="4" cellspacing="0" class="table table-bordered dt-responsive display">',
        );
        $this->table->set_template($template);
        $this->table->set_heading('No','nama item','nama pemasok','mitra','jumlah','harga beli','tgl kirim','');
        $i=1;
        foreach($get_data->result() as $row){
            $btn=array("data"=>anchor(site_url("kirimitem/sunting?id_kirim_item=".$row->id_kirim_item),"<i class='fa fa-eye'></i>",["class"=>"btn btn-xs btn-warning btn-kirim","onclick"=>false]),"width"=>"20px","align"=>"center");
            $this->table->add_row(array("data"=>$i,"width"=>"50px","align"=>"center"),
                                                        $row->nama_item,
                                                        $row->nama_pemasok,
                                                        $row->nama_mitra,
                                                        $row->jumlah,
                                                        $row->harga_beli,
                                                        $row->tgl_kirim,
                                                        $btn);
            $i++;
        }
        echo $this->table->generate();
    ?>
    </div>
</div>
</div>