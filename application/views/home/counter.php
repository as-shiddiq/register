<div class="row">
    <div class="col-lg-3 col-6">
    <!-- small box -->
    <div class="small-box bg-info">
        <div class="inner">
        <h3>1</h3>

        <p>Register Barang</p>
        </div>
        <div class="icon">
        <i class="fa fa-dropbox"></i>
        </div>
    </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-6">
    <!-- small box -->
    <div class="small-box bg-success">
        <div class="inner">
        <h3>112</h3>

        <p>Register Perkara</p>
        </div>
        <div class="icon">
        <i class="fa fa-gg-circle"></i>
        </div>
    </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-6">
    <!-- small box -->
    <div class="small-box bg-warning">
        <div class="inner">
        <h3>10</h3>
        <p>Register Tahanan</p>
        </div>
        <div class="icon">
        <i class="fa fa-align-justify"></i>
        </div>
    </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-6">
    <!-- small box -->
    <div class="small-box bg-danger">
        <div class="inner">
        <h3>230</h3>

        <p>Akses</p>
        </div>
        <div class="icon">
        <i class="fa fa-link"></i>
        </div>
    </div>
    </div>
    <!-- ./col -->
</div>