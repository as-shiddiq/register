<script src="<?=base_url('assets/plugins/Highcharts-6.0.4/code/highcharts.js') ?>"></script>
<script src="<?=base_url('assets/plugins/Highcharts-6.0.4/code/modules/data.js') ?>"></script>
<script src="<?=base_url('assets/plugins/Highcharts-6.0.4/code/modules/exporting.js') ?>"></script>
<script src="<?=base_url('assets/plugins/Highcharts-6.0.4/code/modules/offline-exporting.js') ?>"></script>

<script type="text/javascript">

Highcharts.chart('container-akses', {
    data: {
        table: 'table-akses'
    },
    chart: {
        type: 'spline'
    },
    title: {
        text: ''
    },
    colors: ['#058DC7', '#50B432', '#ED561B', '#DDDF00', '#24CBE5', '#64E572', 
             '#FF9655', '#FFF263', '#6AF9C4'],
    plotOptions: {
        area: {
            stacking: 'normal',
            lineColor: '#666666',
            lineWidth: 1,
            marker: {
                lineWidth: 1,
                lineColor: '#666666'
            }
        }
    },
    legend: {
        layout: 'horizontal',
        align: 'center',
        verticalAlign: 'top'
    },
    yAxis: {
        startOnTick: true,
        endOnTick: false,
        maxPadding: 0.35,
        title: {
            text: null
        },
        labels: {
            format: '{value} m'
        }
    },
    credits: {
        enabled: false
    },
    exporting: {
        enabled: false
    },
    tooltip: {
        formatter: function () {
            return '<b>' + this.series.name + '</b><br/>' +
                this.point.y + ' kali' ;
        }
    },

    responsive: {
        rules: [{
            condition: {
                maxWidth: 500
            },
            chartOptions: {
                legend: {
                    layout: 'horizontal',
                    align: 'center',
                    verticalAlign: 'bottom'
                }
            }
        }]
    }
});

</script>