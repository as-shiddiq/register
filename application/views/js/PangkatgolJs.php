
<!-- script form modal -->
<script>
    $(document).on("click",".btn-tambah",function(event){
        $('#form-pangkatgol').find("input,select,textarea").val("");
        var action="<?=site_url('pangkatgol/simpan')?>";
        $('#form-pangkatgol').attr({'action':action});
    });
    $(document).on("click",".btn-sunting",function(event){
        event.preventDefault();
        var url=$(this).attr('href')+'&JSON';
        var action="<?=site_url('pangkatgol/ubah')?>";
        $("#FormModal").modal();
        $('#form-pangkatgol').attr({'action':action});
        $.getJSON( url, function( response ) {
            var data =response.data;
            $("#form-pangkatgol [name=id_pangkat_gol]").val(data.id_pangkat_gol);
            $("#form-pangkatgol [name=gol]").val(data.gol);
            $("#form-pangkatgol [name=pangkat]").val(data.pangkat);
            $("#form-pangkatgol [name=urutan]").val(data.urutan);
        });
    })
</script>