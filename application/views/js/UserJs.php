
<!-- script form modal -->
<script>
    $(document).on("click",".btn-tambah",function(event){
        $('#form-user').find("input,select,textarea").val("");
        var action="<?=site_url('user/simpan')?>";
        $('#form-user').attr({'action':action});
    });
    $(document).on("click",".btn-sunting",function(event){
        event.preventDefault();
        var url=$(this).attr('href')+'&JSON';
        var action="<?=site_url('user/ubah')?>";
        $("#FormModal").modal();
        $('#form-user').attr({'action':action});
        $.getJSON( url, function( response ) {
            var data =response.data;
            $("#form-user [name=id_user]").val(data.id_user);
            $("#form-user [name=username]").val(data.username);
            $("#form-user [name=password]").val(data.password);
            $("#form-user [name=nickname]").val(data.nickname);
            $("#form-user [name=id_karyawan]").val(data.id_karyawan);
            $("#form-user [name=id_pemasok]").val(data.id_pemasok);
            $("#form-user [name=level]").val(data.level);
        });
    });
    $(function(){
        checkLevel();
        $("[name=level]").change(checkLevel);
    });
    function checkLevel(){
        $level=$("[name=level]").val();
        if($level=='pemasok'){
            $('.pemasok').show();
            $('.kantor').hide();
        }
        else{
            $(".kantor").show();
            $('.pemasok').hide();
        }
    }
</script>