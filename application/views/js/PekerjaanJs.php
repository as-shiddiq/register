
<!-- script form modal -->
<script>
    $(document).on("click",".btn-tambah",function(event){
        $('#form-pekerjaan').find("input,select,textarea").val("");
        var action="<?=site_url('pekerjaan/simpan')?>";
        $('#form-pekerjaan').attr({'action':action});
    });
    $(document).on("click",".btn-sunting",function(event){
        event.preventDefault();
        var url=$(this).attr('href')+'&JSON';
        var action="<?=site_url('pekerjaan/ubah')?>";
        $("#FormModal").modal();
        $('#form-pekerjaan').attr({'action':action});
        $.getJSON( url, function( response ) {
            var data =response.data;
            $("#form-pekerjaan [name=id_pekerjaan]").val(data.id_pekerjaan);
            $("#form-pekerjaan [name=nama_pekerjaan]").val(data.nama_pekerjaan);
        });
    })
</script>