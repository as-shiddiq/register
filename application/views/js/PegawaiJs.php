
<!-- script form modal -->
<script>
    $(document).on("click",".btn-tambah",function(event){
        $('#form-pegawai').find("input,select,textarea").val("");
        var action="<?=site_url('pegawai/simpan')?>";
        $('#form-pegawai').attr({'action':action});
    });
    $(document).on("click",".btn-sunting",function(event){
        event.preventDefault();
        var url=$(this).attr('href')+'&JSON';
        var action="<?=site_url('pegawai/ubah')?>";
        $("#FormModal").modal();
        $('#form-pegawai').attr({'action':action});
        $.getJSON( url, function( response ) {
            var data =response.data;
            $("#form-pegawai [name=id_pegawai]").val(data.id_pegawai);
            $("#form-pegawai [name=nip]").val(data.nip);
            $("#form-pegawai [name=nama_pegawai]").val(data.nama_pegawai);
            $("#form-pegawai [name=id_pangkat_gol]").val(data.id_pangkat_gol);
        });
    })
</script>