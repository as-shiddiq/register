<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
	<!-- form -->
<!-- The Modal -->
<div class="modal fade" id="FormModal">
<form class="validate form-horizontal" id="form-user" method="POST">
  <div class="modal-dialog">
    <div class="modal-content">
      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Form Pengguna</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <!-- Modal body -->
      <div class="modal-body">
        <?=input_hidden('id_user',(isset($id_user)?$id_user:''),'md-input','required');?>						
<div class="form-group">
	<div class="row">
		<div class="col-md-3">
		<label>Username</label>
		</div>
		<div class="col-md-9">
			<?=input_text('username',(isset($username)?$username:''),'md-input','required');?>
		</div>
	</div>
</div>						
<div class="form-group">
	<div class="row">
		<div class="col-md-3">
		<label>Password</label>
		</div>
		<div class="col-md-9">
			<?=input_text('password',(isset($password)?$password:''),'md-input','required');?>
		</div>
	</div>
</div>						
<div class="form-group">
	<div class="row">
		<div class="col-md-3">
		<label>Nickname</label>
		</div>
		<div class="col-md-9">
			<?=input_text('nickname',(isset($nickname)?$nickname:''),'md-input','required');?>
		</div>
	</div>
</div>		
	
<!--endform-->
      </div>
       <!-- Modal footer -->
      <div class="modal-footer">
         <button type="submit" name="simpan" class="btn btn-primary" value="true">Simpan</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal">Tutup</button>
      </div>
    </div>
  </div>
</form>
</div><!--OPEN TABLE-->
<div class="card">
	<div class="card-header">
		<h3 class="card-title">DATA PENGGUNA</h3>

		<div class="card-tools">
		<button type="button" class="btn btn-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
			<i class="fa fa-minus"></i></button>
		</div>
	</div>
	<div class="card-body">

					<!-- table -->
					<button type="button" class="btn btn-success btn-tambah" data-toggle="modal" data-target="#FormModal">
						<i class='fa fa-plus'></i> Tambah
					</button>					
					<hr>
					<?php echo $this->session->flashdata('info');?>
					<?php echo $table;?>
					<!-- end table -->
	</div>
</div>


