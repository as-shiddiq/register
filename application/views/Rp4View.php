<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
  <?php if(isset($_GET['tambah']) OR isset($_GET['ubah'])){ ?>    <!--OPEN FORM -->
  <div class="card">
	<div class="card-header">
		<h3 class="card-title">FORM RP4</h3>

		<div class="card-tools">
		<button type="button" class="btn btn-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
			<i class="fa fa-minus"></i></button>
		</div>
	</div>
	<div class="card-body">

	<!-- form -->
<?php
if(isset($_GET['ubah'])){
  $parameter='ubah';
  $where=array(
          	'no_rp4'=>$_GET['no_rp4']
	        );
  $row=$this->db->get_where('rp4',$where)->row_array();
  extract($row);
}
else{
  $parameter='simpan';
}
?>
<form class="validate form-horizontal" id="form-rp4" method="POST" action="<?=site_url()?>rp4/<?=$parameter;?>">
<div class="col-md-12">
	<div class="row">
			<div class="col-lg-6">
				<div class="form-group">
					<label>No Urut Register</label>
					<?=input_text('no_rp4',($no_rp4??''),'md-input','required');?>
				</div>
			</div>
									
			<div class="col-lg-6">
				<div class="form-group">
					<label>Tanggal</label>
					<?=input_date('tgl',($tgl??''),'md-input','required');?>
				</div>
			</div>
	</div>
</div>		
<div class="col-lg-12">
	<div class="form-group">
		<label>Nama Alamat Yang Dipanggil</label>
		<?=input_text('alamat_dipanggil',($alamat_dipanggil??''),'md-input','required');?>
	</div>
</div>
<?=input_hidden('alasan_pemanggilan',($alasan_pemanggilan??''),'','required');?>						
<div class="col-lg-12">
	<div class="form-group">
		<label>Nomor Register Perkara Tahap LID/DIK</label>
		<?php 
			$op=NULL;
			$op['']='Pilih Salah Satu';  
			$this->db->order_by('tgl_reg_perkara','ASC');
			$data=$this->db->get('rp3');
			foreach($data->result() as $row){
				$op[$row->no_rp3]=$row->tgl_reg_perkara;
			}
			echo select('no_rp3',$op,($no_rp3??''),'','required');?>
	</div>
</div>
	<div class="col-md-12">
			<div class="row">
										
				<div class="col-lg-4">
					<div class="form-group">
						<label>Tempat Menghadap</label>
						<?=input_text('tmpt_menghadap',($tmpt_menghadap??''),'md-input','required');?>
					</div>
				</div>
										
				<div class="col-lg-4">
					<div class="form-group">
						<label>Tanggal</label>
						<?=input_date('tgl_menghadap',($tgl_menghadap??''),'md-input','required');?>
					</div>
				</div>

				<div class="col-lg-4">
					<div class="form-group">
						<label>Jam</label>
						<?=input_text('jam_menghadap',($jam_menghadap??''),'md-input','required');?>
					</div>
				</div>
		</div>
	</div>				
<div class="col-lg-12">
	<div class="form-group">
		<label>Menghadap Kepada</label>
		<?=input_text('menghadap_kpd',($menghadap_kpd??''),'md-input','required');?>
	</div>
</div>
						
<div class="col-lg-12">
	<div class="form-group">
		<label>Keterangan</label>
		<?=textarea('keterangan',($keterangan??''),'md-input','required');?>
	</div>
</div>
<?=input_hidden('tgl_simpan',($tgl_simpan??''),'','required');?><?=input_hidden('no_agenda',($no_agenda??''),'','required');?><!--endform-->
  <div class="col-lg-12">
    <div class="form-group">
      <label></label>
      <button type="submit" name="simpan" class="btn btn-primary" value="true">Simpan</button>
      <a href="javascript:history.go(-1)" class="btn btn-danger">Batal</a>
    </div>
  </div>
  <div class="clearfix"></div>
  </div>
</form>
  	</div>
</div>

  <?php } else { ?>
<!--OPEN TABLE-->
<div class="card">
	<div class="card-header">
		<h3 class="card-title">DATA RP4</h3>

		<div class="card-tools">
		<button type="button" class="btn btn-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
			<i class="fa fa-minus"></i></button>
		</div>
	</div>
	<div class="card-body">

					<!-- table -->
		<?php echo anchor(site_url('rp4?tambah'),"<i class='fa fa-plus'></i> Tambah",['class'=>'btn btn-success btn-tambah']);?>
					
					<hr>
					<?php echo $this->session->flashdata('info');?>
					<?php echo $table;?>
					<!-- end table -->
	</div>
</div>

  <?php }

