<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
  <?php if(isset($_GET['tambah']) OR isset($_GET['ubah'])){ ?>    <!--OPEN FORM -->
  <div class="card">
	<div class="card-header">
		<h3 class="card-title">FORM RT1</h3>

		<div class="card-tools">
		<button type="button" class="btn btn-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
			<i class="fa fa-minus"></i></button>
		</div>
	</div>
	<div class="card-body">

	<!-- form -->
<?php
if(isset($_GET['ubah'])){
  $parameter='ubah';
  $where=array(
          	'no_rt1'=>$_GET['no_rt1']
	        );
  $row=$this->db->get_where('rt1',$where)->row_array();
  extract($row);
}
else{
  $parameter='simpan';
}
?>
<form class="validate form-horizontal" id="form-rt1" method="POST" action="<?=site_url()?>rt1/<?=$parameter;?>">
  
<div class="col-lg-12">
	<div class="form-group">
		<label>No Rt1</label>
		<?=input_text('no_rt1',($no_rt1??''),'md-input','required');?>
	</div>
</div>
						
<div class="col-lg-12">
	<div class="form-group">
		<label>Identitas Tersangka</label>
		<?php 
			$op=NULL;
			$op['']='Pilih Salah Satu';  
			$this->db->order_by('nama_lengkap','ASC');
			$data=$this->db->get('biodata');
			foreach($data->result() as $row){
				$op[$row->nik]=$row->nama_lengkap;
			}
			echo select('nik',$op,($nik??''),'','required');?>
	</div>
</div>
						
<div class="col-lg-12">
	<div class="form-group">
		<label>No Rp3</label>
		<?php 
			$op=NULL;
			$op['']='Pilih Salah Satu';  
			$this->db->order_by('tgl_reg_perkara','ASC');
			$data=$this->db->get('rp3');
			foreach($data->result() as $row){
				$op[$row->no_rp3]=$row->tgl_reg_perkara;
			}
			echo select('no_rp3',$op,($no_rp3??''),'','required');?>
	</div>
</div>
						
<div class="col-lg-12">
	<div class="form-group">
		<label>Pasal Yang Disangkakan</label>
		<?=input_text('pasal_disangkakan',($pasal_disangkakan??''),'md-input','required');?>
	</div>
</div>
						
<div class="col-lg-12">
	<div class="form-group">
		<label>Jenis Penahanan</label>
		<?=input_text('jns_penahanan',($jns_penahanan??''),'md-input','required');?>
	</div>
</div>
						
<div class="col-lg-12">
	<div class="form-group">
		<label>Ditahan Sejak Tgl</label>
		<?=input_date('tgl_ditahan',($tgl_ditahan??''),'md-input','required');?>
	</div>
</div>
						
<div class="col-lg-12">
	<div class="form-group">
		<label>Psl.24 (2) KUHAP Sejak Tgl.</label>
		<?=input_date('tgl_24kuhap_sejak',($tgl_24kuhap_sejak??''),'md-input','required');?>
	</div>
</div>
						
<div class="col-lg-12">
	<div class="form-group">
		<label>Psl. 29 KUHAP Sejak Tgl.</label>
		<?=input_date('tgl_29kuhap_sejak',($tgl_29kuhap_sejak??''),'md-input','required');?>
	</div>
</div>
						
<div class="col-lg-12">
	<div class="form-group">
		<label>Pengalihan Jenis Penahanan Tgl.</label>
		<?=input_date('tgl_pengalihan',($tgl_pengalihan??''),'md-input','required');?>
	</div>
</div>
						
<div class="col-lg-12">
	<div class="form-group">
		<label>Dikeluarkan/Keluar Demi Hukum</label>
		<?=input_text('dikeluarkan_demi_hukum',($dikeluarkan_demi_hukum??''),'md-input','required');?>
	</div>
</div>
						
<div class="col-lg-12">
	<div class="form-group">
		<label>Penangguhan No</label>
		<?=input_text('no_pengguhan',($no_pengguhan??''),'md-input','required');?>
	</div>
</div>
						
<div class="col-lg-12">
	<div class="form-group">
		<label>Penangguhan Tgl.</label>
		<?=input_date('tgl_penangguhan',($tgl_penangguhan??''),'md-input','required');?>
	</div>
</div>
						
<div class="col-lg-12">
	<div class="form-group">
		<label>Tanpa/Dengan Jaminan</label>
		<?=input_text('jaminan_penangguhan',($jaminan_penangguhan??''),'md-input','required');?>
	</div>
</div>
						
<div class="col-lg-12">
	<div class="form-group">
		<label>Pencabutan Sejak Tgl.</label>
		<?=input_date('tgl_pencabutan_penangguhan',($tgl_pencabutan_penangguhan??''),'md-input','required');?>
	</div>
</div>
						
<div class="col-lg-12">
	<div class="form-group">
		<label>Penyelesaian</label>
		<?=input_text('penyelesaian',($penyelesaian??''),'md-input','required');?>
	</div>
</div>
						
<div class="col-lg-12">
	<div class="form-group">
		<label>Keterangan</label>
		<?=textarea('keterangan',($keterangan??''),'md-input','required');?>
	</div>
</div>
<?=input_hidden('tgl_simpan',($tgl_simpan??''),'','required');?><!--endform-->
  <div class="col-lg-12">
    <div class="form-group">
      <label></label>
      <button type="submit" name="simpan" class="btn btn-primary" value="true">Simpan</button>
      <a href="javascript:history.go(-1)" class="btn btn-danger">Batal</a>
    </div>
  </div>
  <div class="clearfix"></div>
  </div>
</form>
  	</div>
</div>

  <?php } else { ?>
<!--OPEN TABLE-->
<div class="card">
	<div class="card-header">
		<h3 class="card-title">DATA RT1</h3>

		<div class="card-tools">
		<button type="button" class="btn btn-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
			<i class="fa fa-minus"></i></button>
		</div>
	</div>
	<div class="card-body">

					<!-- table -->
		<?php echo anchor(site_url('rt1?tambah'),"<i class='fa fa-plus'></i> Tambah",['class'=>'btn btn-success btn-tambah']);?>
					
					<hr>
					<?php echo $this->session->flashdata('info');?>
					<div style="width:100%;overflow:scroll">
						<?php echo $table;?>
					</div>
					<!-- end table -->
	</div>
</div>

  <?php }

