<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Rp4Model extends CI_Model {
function get_data(){
  $data=$this->db->select('a.*,b.tgl_reg_perkara')
        ->from('rp4 a')
        ->join('rp3 b','a.no_rp3=b.no_rp3','left')
        ->order_by('no_rp4','ASC')
        ->get();
  return $data;
}

function insert($data){
  $this->db->insert('rp4',$data);
  $this->session->set_flashdata('info',info_success(icon('check').' Data Sukses Disimpan'));
}

function update($data,$where){
  $cek=$this->db->get_where('rp4',$where);
  if($cek->num_rows()>0){
    $this->db->update('rp4',$data,$where);
    $this->session->set_flashdata('info',info_success(icon('check').' Data Sukses Diubah'));
  }
  else{
    $this->session->set_flashdata('info',info_danger(icon('times').' Gagal Sukses Diubah [\'data tidak ditemukan\']'));
  }
}

function delete($where){
  $this->db->delete('rp4',$where);
  $this->session->set_flashdata('info',info_success(icon('check').' Data Sukses Dihapus'));
}
//end class
}
