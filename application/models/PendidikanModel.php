<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class PendidikanModel extends CI_Model {
function get_data(){
  $data=$this->db->select('*')
              ->from('pendidikan')
              ->order_by('id_pendidikan','ASC')
              ->get();
  return $data;
}

function insert($data){
  $this->db->insert('pendidikan',$data);
  $this->session->set_flashdata('info',info_success(icon('check').' Data Sukses Disimpan'));
}

function update($data,$where){
  $cek=$this->db->get_where('pendidikan',$where);
  if($cek->num_rows()>0){
    $this->db->update('pendidikan',$data,$where);
    $this->session->set_flashdata('info',info_success(icon('check').' Data Sukses Diubah'));
  }
  else{
    $this->session->set_flashdata('info',info_danger(icon('times').' Gagal Sukses Diubah [\'data tidak ditemukan\']'));
  }
}

function delete($where){
  $this->db->delete('pendidikan',$where);
  $this->session->set_flashdata('info',info_success(icon('check').' Data Sukses Dihapus'));
}
//end class
}
