<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class PangkatgolModel extends CI_Model {
function get_data(){
  $data=$this->db->select('*')
              ->from('pangkat_gol')
              ->order_by('id_pangkat_gol','ASC')
              ->get();
  return $data;
}

function insert($data){
  $this->db->insert('pangkat_gol',$data);
  $this->session->set_flashdata('info',info_success(icon('check').' Data Sukses Disimpan'));
}

function update($data,$where){
  $cek=$this->db->get_where('pangkat_gol',$where);
  if($cek->num_rows()>0){
    $this->db->update('pangkat_gol',$data,$where);
    $this->session->set_flashdata('info',info_success(icon('check').' Data Sukses Diubah'));
  }
  else{
    $this->session->set_flashdata('info',info_danger(icon('times').' Gagal Sukses Diubah [\'data tidak ditemukan\']'));
  }
}

function delete($where){
  $this->db->delete('pangkat_gol',$where);
  $this->session->set_flashdata('info',info_success(icon('check').' Data Sukses Dihapus'));
}
//end class
}
