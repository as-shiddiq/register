<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Rt2Model extends CI_Model {
function get_data(){
  $data=$this->db->select('a.*,b.nama_lengkap')
        ->from('rt2 a')
        ->join('biodata b','a.nik=b.nik','left')
        ->order_by('no_rt2','ASC')
        ->get();
  return $data;
}

function insert($data){
  $this->db->insert('rt2',$data);
  $this->session->set_flashdata('info',info_success(icon('check').' Data Sukses Disimpan'));
}

function update($data,$where){
  $cek=$this->db->get_where('rt2',$where);
  if($cek->num_rows()>0){
    $this->db->update('rt2',$data,$where);
    $this->session->set_flashdata('info',info_success(icon('check').' Data Sukses Diubah'));
  }
  else{
    $this->session->set_flashdata('info',info_danger(icon('times').' Gagal Sukses Diubah [\'data tidak ditemukan\']'));
  }
}

function delete($where){
  $this->db->delete('rt2',$where);
  $this->session->set_flashdata('info',info_success(icon('check').' Data Sukses Dihapus'));
}
//end class
}
