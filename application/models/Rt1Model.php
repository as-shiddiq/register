<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Rt1Model extends CI_Model {
function get_data(){
  $data=$this->db->select('a.*,b.nama_lengkap,c.tgl_reg_perkara')
        ->from('rt1 a')
        ->join('biodata b','a.nik=b.nik','left')
				->join('rp3 c','a.no_rp3=c.no_rp3','left')
        ->order_by('no_rt1','ASC')
        ->get();
  return $data;
}

function insert($data){
  $this->db->insert('rt1',$data);
  $this->session->set_flashdata('info',info_success(icon('check').' Data Sukses Disimpan'));
}

function update($data,$where){
  $cek=$this->db->get_where('rt1',$where);
  if($cek->num_rows()>0){
    $this->db->update('rt1',$data,$where);
    $this->session->set_flashdata('info',info_success(icon('check').' Data Sukses Diubah'));
  }
  else{
    $this->session->set_flashdata('info',info_danger(icon('times').' Gagal Sukses Diubah [\'data tidak ditemukan\']'));
  }
}

function delete($where){
  $this->db->delete('rt1',$where);
  $this->session->set_flashdata('info',info_success(icon('check').' Data Sukses Dihapus'));
}
//end class
}
