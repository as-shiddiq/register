<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Rp2Model extends CI_Model {
function get_data(){
  $data=$this->db->select('a.*,b.nip')
        ->from('rp2 a')
        ->join('pegawai b','a.id_pegawai=b.id_pegawai','left')
        ->order_by('no_rp2','ASC')
        ->get();
  return $data;
}

function insert($data){
  $this->db->insert('rp2',$data);
  $this->session->set_flashdata('info',info_success(icon('check').' Data Sukses Disimpan'));
}

function update($data,$where){
  $cek=$this->db->get_where('rp2',$where);
  if($cek->num_rows()>0){
    $this->db->update('rp2',$data,$where);
    $this->session->set_flashdata('info',info_success(icon('check').' Data Sukses Diubah'));
  }
  else{
    $this->session->set_flashdata('info',info_danger(icon('times').' Gagal Sukses Diubah [\'data tidak ditemukan\']'));
  }
}

function delete($where){
  $this->db->delete('rp2',$where);
  $this->session->set_flashdata('info',info_success(icon('check').' Data Sukses Dihapus'));
}
//end class
}
