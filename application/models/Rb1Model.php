<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Rb1Model extends CI_Model {
function get_data(){
  $data=$this->db->select('a.*,b.tgl_reg_perkara,b.no_rp3,c.nama_lengkap')
        ->from('rb1 a')
        ->join('rp3 b','a.no_rp3=b.no_rp3','left')
        ->join('biodata c','b.nik=c.nik','left')
        ->order_by('no_rb1','ASC')
        ->get();
  return $data;
}

function insert($data){
  $this->db->insert('rb1',$data);
  $this->session->set_flashdata('info',info_success(icon('check').' Data Sukses Disimpan'));
}

function update($data,$where){
  $cek=$this->db->get_where('rb1',$where);
  if($cek->num_rows()>0){
    $this->db->update('rb1',$data,$where);
    $this->session->set_flashdata('info',info_success(icon('check').' Data Sukses Diubah'));
  }
  else{
    $this->session->set_flashdata('info',info_danger(icon('times').' Gagal Sukses Diubah [\'data tidak ditemukan\']'));
  }
}

function delete($where){
  $this->db->delete('rb1',$where);
  $this->session->set_flashdata('info',info_success(icon('check').' Data Sukses Dihapus'));
}
//end class
}
