<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class PegawaiModel extends CI_Model {
function get_data(){
  $data=$this->db->select('a.*,b.gol,b.pangkat')
        ->from('pegawai a')
        ->join('pangkat_gol b','a.id_pangkat_gol=b.id_pangkat_gol','left')
        ->order_by('id_pegawai','ASC')
        ->get();
  return $data;
}

function insert($data){
  $this->db->insert('pegawai',$data);
  $this->session->set_flashdata('info',info_success(icon('check').' Data Sukses Disimpan'));
}

function update($data,$where){
  $cek=$this->db->get_where('pegawai',$where);
  if($cek->num_rows()>0){
    $this->db->update('pegawai',$data,$where);
    $this->session->set_flashdata('info',info_success(icon('check').' Data Sukses Diubah'));
  }
  else{
    $this->session->set_flashdata('info',info_danger(icon('times').' Gagal Sukses Diubah [\'data tidak ditemukan\']'));
  }
}

function delete($where){
  $this->db->delete('pegawai',$where);
  $this->session->set_flashdata('info',info_success(icon('check').' Data Sukses Dihapus'));
}
//end class
}
