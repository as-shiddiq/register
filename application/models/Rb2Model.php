<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Rb2Model extends CI_Model {
function get_data(){
  $data=$this->db->select('a.*,b.reg_perkara')
        ->from('rb2 a')
        ->join('rb2 b','a.no_rb2=b.no_rb2','left')
        ->order_by('no_rb2','ASC')
        ->get();
  return $data;
}

function insert($data){
  $this->db->insert('rb2',$data);
  $this->session->set_flashdata('info',info_success(icon('check').' Data Sukses Disimpan'));
}

function update($data,$where){
  $cek=$this->db->get_where('rb2',$where);
  if($cek->num_rows()>0){
    $this->db->update('rb2',$data,$where);
    $this->session->set_flashdata('info',info_success(icon('check').' Data Sukses Diubah'));
  }
  else{
    $this->session->set_flashdata('info',info_danger(icon('times').' Gagal Sukses Diubah [\'data tidak ditemukan\']'));
  }
}

function delete($where){
  $this->db->delete('rb2',$where);
  $this->session->set_flashdata('info',info_success(icon('check').' Data Sukses Dihapus'));
}
//end class
}
