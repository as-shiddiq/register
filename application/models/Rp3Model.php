<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Rp3Model extends CI_Model {
function get_data(){
  $data=$this->db->select('a.*,b.nama_lengkap,c.nip,c.nama_pegawai')
        ->from('rp3 a')
        ->join('biodata b','a.nik=b.nik','left')
				->join('pegawai c','a.id_pegawai=c.id_pegawai','left')
        ->order_by('no_rp3','ASC')
        ->get();
  return $data;
}

function insert($data){
  $this->db->insert('rp3',$data);
  $this->session->set_flashdata('info',info_success(icon('check').' Data Sukses Disimpan'));
}

function update($data,$where){
  $cek=$this->db->get_where('rp3',$where);
  if($cek->num_rows()>0){
    $this->db->update('rp3',$data,$where);
    $this->session->set_flashdata('info',info_success(icon('check').' Data Sukses Diubah'));
  }
  else{
    $this->session->set_flashdata('info',info_danger(icon('times').' Gagal Sukses Diubah [\'data tidak ditemukan\']'));
  }
}

function delete($where){
  $this->db->delete('rp3',$where);
  $this->session->set_flashdata('info',info_success(icon('check').' Data Sukses Dihapus'));
}
//end class
}
