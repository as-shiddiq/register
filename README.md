## Deskripsi
## Spesification
1. PHP 7.2 (CodeIgniter 3.1.8) | With Generator
2. MySQL 15.1 Distrib 10.1.29-MariaDB
3. jQuery v1.12.4
4. Bootstrap v4.0.0

## Template
1. AdminLTE 3 Alpha (https://github.com/almasaeed2010/AdminLTE/archive/v3.0.0-alpha.zip)


##### Original by [Nasrullah Siddik](https://t.me/as_shiddiq)
